PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOIC127P700X210-16N
$EndINDEX
$MODULE SOIC127P700X210-16N
Po 0 0 0 15 00000000 00000000 ~~
Li SOIC127P700X210-16N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.74071 -6.68548 1.09836 1.09836 0 0.05 N V 21 "SOIC127P700X210-16N"
T1 -1.03671 6.64365 1.09149 1.09149 0 0.05 N V 21 "VAL**"
DC -4.955 -5.035 -4.755 -5.035 0 21
DC -4.955 -5.035 -4.755 -5.035 0 24
DS -2.2 -5.15 2.2 -5.15 0.127 24
DS -2.2 5.15 2.2 5.15 0.127 24
DS -2.2 -5.15 2.2 -5.15 0.127 21
DS -2.2 5.15 2.2 5.15 0.127 21
DS -2.2 -5.15 -2.2 5.15 0.127 24
DS 2.2 -5.15 2.2 5.15 0.127 24
DS -4.305 -5.4 4.305 -5.4 0.05 26
DS -4.305 5.4 4.305 5.4 0.05 26
DS -4.305 -5.4 -4.305 5.4 0.05 26
DS 4.305 -5.4 4.305 5.4 0.05 26
$PAD
Sh "1" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 -4.445
$EndPAD
$PAD
Sh "2" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 -3.175
$EndPAD
$PAD
Sh "3" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 -1.905
$EndPAD
$PAD
Sh "4" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 -0.635
$EndPAD
$PAD
Sh "5" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 0.635
$EndPAD
$PAD
Sh "6" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 1.905
$EndPAD
$PAD
Sh "7" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 3.175
$EndPAD
$PAD
Sh "8" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.155 4.445
$EndPAD
$PAD
Sh "9" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 4.445
$EndPAD
$PAD
Sh "10" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 3.175
$EndPAD
$PAD
Sh "11" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 1.905
$EndPAD
$PAD
Sh "12" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 0.635
$EndPAD
$PAD
Sh "13" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 -0.635
$EndPAD
$PAD
Sh "14" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 -1.905
$EndPAD
$PAD
Sh "15" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 -3.175
$EndPAD
$PAD
Sh "16" R 1.8 0.59 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.155 -4.445
$EndPAD
$EndMODULE SOIC127P700X210-16N
