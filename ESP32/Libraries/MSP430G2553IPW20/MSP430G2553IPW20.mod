PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
TSSOP-20
$EndINDEX
$MODULE TSSOP-20
Po 0 0 0 15 00000000 00000000 ~~
Li TSSOP-20
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.59139 4.05011 0.240148 0.240148 0 0.05 N V 21 "TSSOP-20"
T1 -1.43869 4.69984 0.240115 0.240115 0 0.05 N V 21 "VAL**"
DS -2.2 -3.2 -2.2 3.4 0.1 24
DS -2.2 3.4 2.3 3.4 0.1 24
DS 2.3 3.4 2.3 -3.2 0.1 24
DS 2.3 -3.2 -2.2 -3.2 0.1 24
DC -1.35 -2.6 -1.05845 -2.6 0 24
$PAD
Sh "1" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 -2.8218
$EndPAD
$PAD
Sh "2" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 -2.1718
$EndPAD
$PAD
Sh "3" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 -1.5218
$EndPAD
$PAD
Sh "4" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 -0.8718
$EndPAD
$PAD
Sh "5" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 -0.2218
$EndPAD
$PAD
Sh "6" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 0.4282
$EndPAD
$PAD
Sh "7" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 1.0782
$EndPAD
$PAD
Sh "8" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 1.7282
$EndPAD
$PAD
Sh "9" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 2.3782
$EndPAD
$PAD
Sh "10" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.7475 3.0282
$EndPAD
$PAD
Sh "20" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 -2.8218
$EndPAD
$PAD
Sh "19" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 -2.1718
$EndPAD
$PAD
Sh "18" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 -1.5218
$EndPAD
$PAD
Sh "17" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 -0.8718
$EndPAD
$PAD
Sh "16" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 -0.2218
$EndPAD
$PAD
Sh "15" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 0.4282
$EndPAD
$PAD
Sh "14" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 1.0782
$EndPAD
$PAD
Sh "13" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 1.7282
$EndPAD
$PAD
Sh "12" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 2.3782
$EndPAD
$PAD
Sh "11" R 1.6 0.3 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.8525 3.0282
$EndPAD
$EndMODULE TSSOP-20
