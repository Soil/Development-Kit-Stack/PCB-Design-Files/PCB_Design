EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lora
LIBS:MSP432
LIBS:esda6v8v6
LIBS:gprsquectel
LIBS:Mother_Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Hub Mother Board"
Date "2018-06-22"
Rev "v1.0"
Comp "Soil Agritech Pvt. Ltd."
Comment1 "Designed by SRS"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MSP432 U?
U 1 1 5B2D1944
P 6050 3350
F 0 "U?" H 7150 2850 60  0000 C CNN
F 1 "MSP432" H 5100 3950 60  0000 C CNN
F 2 "" H 4950 3250 60  0001 C CNN
F 3 "" H 4950 3250 60  0001 C CNN
	1    6050 3350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
