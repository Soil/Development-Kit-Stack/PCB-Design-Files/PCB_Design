EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Opto-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Three Phase Board"
Date "2018-05-04"
Rev "v1.0"
Comp "Soil Agritech Pvt. Ltd."
Comment1 "Designed by Swaroop Sadarjoshi"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02 J4
U 1 1 5AEB52D8
P 7400 3250
F 0 "J4" H 7400 3350 50  0000 C CNN
F 1 "Conn_01x02" H 7400 3050 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 7400 3250 50  0001 C CNN
F 3 "" H 7400 3250 50  0001 C CNN
	1    7400 3250
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J5
U 1 1 5AEB52F7
P 7400 4050
F 0 "J5" H 7400 4150 50  0000 C CNN
F 1 "Conn_01x02" H 7400 3850 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 7400 4050 50  0001 C CNN
F 3 "" H 7400 4050 50  0001 C CNN
	1    7400 4050
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J1
U 1 1 5AEB5338
P 4200 3300
F 0 "J1" H 4200 3400 50  0000 C CNN
F 1 "Conn_01x02" H 4200 3100 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 4200 3300 50  0001 C CNN
F 3 "" H 4200 3300 50  0001 C CNN
	1    4200 3300
	-1   0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5AEB5653
P 4700 3350
F 0 "C1" H 4725 3450 50  0000 L CNN
F 1 "0.1uF" H 4725 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4738 3200 50  0001 C CNN
F 3 "" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J3
U 1 1 5AEB528F
P 7400 2550
F 0 "J3" H 7400 2650 50  0000 C CNN
F 1 "Conn_01x02" H 7400 2350 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 7400 2550 50  0001 C CNN
F 3 "" H 7400 2550 50  0001 C CNN
	1    7400 2550
	1    0    0    -1  
$EndComp
$Comp
L LTV-814 U1
U 1 1 5AEB6304
P 6200 2650
F 0 "U1" H 6000 2850 50  0000 L CNN
F 1 "LTV-814" H 6200 2850 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 6000 2450 50  0001 L CIN
F 3 "" H 6225 2650 50  0001 L CNN
	1    6200 2650
	-1   0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5AEB6924
P 6750 2550
F 0 "R1" V 6830 2550 50  0000 C CNN
F 1 "47k" V 6750 2550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_Power_L25.0mm_W9.0mm_P30.48mm" V 6680 2550 50  0001 C CNN
F 3 "" H 6750 2550 50  0001 C CNN
	1    6750 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 2550 7200 2550
Wire Wire Line
	7200 2650 7050 2650
Wire Wire Line
	7050 2650 7050 4250
$Comp
L LTV-814 U2
U 1 1 5AF08B36
P 6200 3350
F 0 "U2" H 6000 3550 50  0000 L CNN
F 1 "LTV-814" H 6200 3550 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 6000 3150 50  0001 L CIN
F 3 "" H 6225 3350 50  0001 L CNN
	1    6200 3350
	-1   0    0    -1  
$EndComp
$Comp
L LTV-814 U3
U 1 1 5AF08D1A
P 6200 4150
F 0 "U3" H 6000 4350 50  0000 L CNN
F 1 "LTV-814" H 6200 4350 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 6000 3950 50  0001 L CIN
F 3 "" H 6225 4150 50  0001 L CNN
	1    6200 4150
	-1   0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5AF08E80
P 6800 3250
F 0 "R2" V 6880 3250 50  0000 C CNN
F 1 "47k" V 6800 3250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_Power_L25.0mm_W9.0mm_P30.48mm" V 6730 3250 50  0001 C CNN
F 3 "" H 6800 3250 50  0001 C CNN
	1    6800 3250
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5AF08ED6
P 6850 4050
F 0 "R3" V 6930 4050 50  0000 C CNN
F 1 "47k" V 6850 4050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_Power_L25.0mm_W9.0mm_P30.48mm" V 6780 4050 50  0001 C CNN
F 3 "" H 6850 4050 50  0001 C CNN
	1    6850 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 3250 6950 3250
Wire Wire Line
	6650 3250 6500 3250
Wire Wire Line
	7050 3350 7200 3350
Connection ~ 7050 2750
Wire Wire Line
	7050 3450 6500 3450
Connection ~ 7050 3350
Wire Wire Line
	7200 4050 7000 4050
Wire Wire Line
	6700 4050 6500 4050
Wire Wire Line
	7050 4150 7200 4150
Connection ~ 7050 3450
Wire Wire Line
	7050 4250 6500 4250
Connection ~ 7050 4150
Wire Wire Line
	4500 3100 4500 3300
Wire Wire Line
	4500 3400 4500 3600
Wire Wire Line
	5700 2750 5700 3250
Wire Wire Line
	5700 3250 5900 3250
Wire Wire Line
	5900 3450 5700 3450
Wire Wire Line
	5700 3450 5700 4050
Wire Wire Line
	5700 4050 5900 4050
Wire Wire Line
	4800 4250 5900 4250
Wire Wire Line
	4800 3600 4800 4250
Wire Wire Line
	4500 3600 4800 3600
Wire Wire Line
	4700 3600 4700 3500
Connection ~ 4700 3600
Wire Wire Line
	4700 3200 4700 3100
Wire Wire Line
	4500 3100 5500 3100
Wire Wire Line
	4800 3100 4800 2550
Connection ~ 4700 3100
Wire Wire Line
	5350 3800 5700 3800
Connection ~ 4800 3100
Text Notes 1700 6600 0    60   ~ 0
2 pin JST Connector 2.54mm
Text Notes 1350 6600 0    60   ~ 0
J1
Text Notes 1000 7050 0    60   ~ 0
J3, J4, J5
Text Notes 1700 7050 0    60   ~ 0
2 way PCB Terminal Block heavy duty 5.08mm
Text Notes 1350 6850 0    60   ~ 0
J2
Text Notes 1700 6850 0    60   ~ 0
2 pin header with jumper cap 2.54mm for 1 single phase
Text Notes 950  7300 0    60   ~ 0
R1, R2, R3
Text Notes 1700 7300 0    60   ~ 0
47kohm, 5W power resistor
Wire Wire Line
	4500 3300 4400 3300
Wire Wire Line
	4500 3400 4400 3400
$Comp
L GND #PWR01
U 1 1 5AF0B0B0
P 5250 4400
F 0 "#PWR01" H 5250 4150 50  0001 C CNN
F 1 "GND" H 5250 4250 50  0000 C CNN
F 2 "" H 5250 4400 50  0001 C CNN
F 3 "" H 5250 4400 50  0001 C CNN
	1    5250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4400 5250 4250
Connection ~ 5250 4250
$Comp
L GND #PWR02
U 1 1 5AF0B2CE
P 5800 6950
F 0 "#PWR02" H 5800 6700 50  0001 C CNN
F 1 "GND" H 5800 6800 50  0000 C CNN
F 2 "" H 5800 6950 50  0001 C CNN
F 3 "" H 5800 6950 50  0001 C CNN
	1    5800 6950
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5AF0B2FA
P 5800 6650
F 0 "#FLG03" H 5800 6725 50  0001 C CNN
F 1 "PWR_FLAG" H 5800 6800 50  0000 C CNN
F 2 "" H 5800 6650 50  0001 C CNN
F 3 "" H 5800 6650 50  0001 C CNN
	1    5800 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 6950 5800 6650
Text Notes 1350 6350 0    60   ~ 0
C1
Text Notes 1700 6350 0    60   ~ 0
0.1uF Ceramic capacitor 16V SMD 1206 X7R
Wire Wire Line
	7050 2750 6500 2750
Wire Wire Line
	6600 2550 6500 2550
Wire Wire Line
	5900 2750 5700 2750
Wire Wire Line
	4800 2550 5900 2550
Wire Wire Line
	5350 3700 5500 3700
Wire Wire Line
	5500 3700 5500 3100
Connection ~ 5700 3800
$Comp
L Conn_01x02 J2
U 1 1 5AF290E6
P 5150 3700
F 0 "J2" H 5150 3800 50  0000 C CNN
F 1 "Conn_01x02" H 5150 3500 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 5150 3700 50  0001 C CNN
F 3 "" H 5150 3700 50  0001 C CNN
	1    5150 3700
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
