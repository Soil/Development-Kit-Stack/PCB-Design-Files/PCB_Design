EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Lora_DRF1276DM
LIBS:MSP432
LIBS:MIC29302AWU-TR
LIBS:IDC_2X5
LIBS:KLDX-0202-AC
LIBS:Mother_Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Hub Mother Board"
Date "2018-06-29"
Rev "v1.0"
Comp "Soil Agritech Pvt. Ltd."
Comment1 "Designed by SRS"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x16 J3
U 1 1 5B35E157
P 1450 4500
F 0 "J3" H 1450 5300 50  0000 C CNN
F 1 "Conn_01x16" H 1450 3600 50  0000 C CNN
F 2 "" H 1450 4500 50  0001 C CNN
F 3 "" H 1450 4500 50  0001 C CNN
	1    1450 4500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J1
U 1 1 5B35E174
P 10050 2550
F 0 "J1" H 10050 2650 50  0000 C CNN
F 1 "Conn_01x02" H 10050 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10050 2550 50  0001 C CNN
F 3 "" H 10050 2550 50  0001 C CNN
	1    10050 2550
	1    0    0    -1  
$EndComp
$Comp
L Jack-DC J2
U 1 1 5B35E419
P 10000 3250
F 0 "J2" H 10000 3460 50  0000 C CNN
F 1 "Jack-DC" H 10000 3075 50  0000 C CNN
F 2 "TerminalBlocks_WAGO:TerminalBlock_WAGO-804_RM5mm_3pol" H 10050 3210 50  0001 C CNN
F 3 "" H 10050 3210 50  0001 C CNN
	1    10000 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
