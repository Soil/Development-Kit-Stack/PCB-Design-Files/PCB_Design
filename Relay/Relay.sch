EESchema Schematic File Version 2
LIBS:Relay-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:idc-2x5
LIBS:Relay-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Relay Schematic"
Date "2018-05-03"
Rev "v1.0"
Comp "Soil Agritech Pvt. Ltd."
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SANYOU_SRD_Form_C K6
U 1 1 5AE9972D
P 8600 1350
F 0 "K6" H 9050 1500 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 9050 1400 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 10050 1300 50  0001 C CNN
F 3 "" H 8600 1350 50  0001 C CNN
	1    8600 1350
	0    1    1    0   
$EndComp
$Comp
L SANYOU_SRD_Form_C K5
U 1 1 5AE99794
P 3950 6950
F 0 "K5" H 4400 7100 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 4400 7000 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 5400 6900 50  0001 C CNN
F 3 "" H 3950 6950 50  0001 C CNN
	1    3950 6950
	0    1    1    0   
$EndComp
$Comp
L SANYOU_SRD_Form_C K4
U 1 1 5AE997DF
P 3900 5350
F 0 "K4" H 4350 5500 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 4350 5400 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 5350 5300 50  0001 C CNN
F 3 "" H 3900 5350 50  0001 C CNN
	1    3900 5350
	0    1    1    0   
$EndComp
$Comp
L SANYOU_SRD_Form_C K3
U 1 1 5AE99844
P 3900 4050
F 0 "K3" H 4350 4200 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 4350 4100 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 5350 4000 50  0001 C CNN
F 3 "" H 3900 4050 50  0001 C CNN
	1    3900 4050
	0    1    1    0   
$EndComp
$Comp
L SANYOU_SRD_Form_C K2
U 1 1 5AE998C8
P 3850 2450
F 0 "K2" H 4300 2600 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 4300 2500 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 5300 2400 50  0001 C CNN
F 3 "" H 3850 2450 50  0001 C CNN
	1    3850 2450
	0    1    1    0   
$EndComp
$Comp
L Screw_Terminal_01x03 J5
U 1 1 5AE9DD2D
P 2300 7000
F 0 "J5" H 2300 7200 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 2300 6800 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 2300 7000 50  0001 C CNN
F 3 "" H 2300 7000 50  0001 C CNN
	1    2300 7000
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J7
U 1 1 5AE9EA49
P 7400 2850
F 0 "J7" H 7400 3050 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 7400 2650 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 7400 2850 50  0001 C CNN
F 3 "" H 7400 2850 50  0001 C CNN
	1    7400 2850
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J6
U 1 1 5AE9EAFE
P 7400 1500
F 0 "J6" H 7400 1700 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 7400 1300 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 7400 1500 50  0001 C CNN
F 3 "" H 7400 1500 50  0001 C CNN
	1    7400 1500
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J2
U 1 1 5AE9EB54
P 2250 2450
F 0 "J2" H 2250 2650 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 2250 2250 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 2250 2450 50  0001 C CNN
F 3 "" H 2250 2450 50  0001 C CNN
	1    2250 2450
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J4
U 1 1 5AE9ED16
P 2300 5350
F 0 "J4" H 2300 5550 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 2300 5150 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 2300 5350 50  0001 C CNN
F 3 "" H 2300 5350 50  0001 C CNN
	1    2300 5350
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J3
U 1 1 5AE9ED1C
P 2300 4100
F 0 "J3" H 2300 4300 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 2300 3900 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 2300 4100 50  0001 C CNN
F 3 "" H 2300 4100 50  0001 C CNN
	1    2300 4100
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x03 J1
U 1 1 5AE9ED22
P 2250 1100
F 0 "J1" H 2250 1300 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 2250 900 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Altech_AK300-3_P5.00mm" H 2250 1100 50  0001 C CNN
F 3 "" H 2250 1100 50  0001 C CNN
	1    2250 1100
	-1   0    0    1   
$EndComp
Text GLabel 7450 4700 0    60   Input ~ 0
+5V
Text GLabel 8450 5100 2    60   Input ~ 0
I7
Text GLabel 8450 5200 2    60   Input ~ 0
I6
Text GLabel 8450 5300 2    60   Input ~ 0
I5
Text GLabel 8450 5400 2    60   Input ~ 0
I4
Text GLabel 8450 5500 2    60   Input ~ 0
I3
Text GLabel 8450 5600 2    60   Input ~ 0
I2
Text GLabel 8450 5700 2    60   Input ~ 0
I1
Text GLabel 7650 5100 0    60   Input ~ 0
O7
Text GLabel 7650 5200 0    60   Input ~ 0
O6
Text GLabel 7650 5300 0    60   Input ~ 0
O5
Text GLabel 7650 5400 0    60   Input ~ 0
O4
Text GLabel 7650 5500 0    60   Input ~ 0
O3
Text GLabel 7650 5600 0    60   Input ~ 0
O2
Text GLabel 7650 5700 0    60   Input ~ 0
O1
$Comp
L IDC-RESCUE-Relay CON1
U 1 1 5AEB0134
P 9900 5350
F 0 "CON1" H 9730 5680 50  0000 C CNN
F 1 "IDC-2x5" H 9710 5020 50  0000 L BNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" V 9480 5370 50  0001 C CNN
F 3 "" H 9900 5350 50  0001 C CNN
	1    9900 5350
	1    0    0    -1  
$EndComp
Text GLabel 8900 2700 2    60   Input ~ 0
+5V
$Comp
L SANYOU_SRD_Form_C K7
U 1 1 5AE994A5
P 8600 2900
F 0 "K7" H 9050 3050 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 9050 2950 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 10050 2850 50  0001 C CNN
F 3 "" H 8600 2900 50  0001 C CNN
	1    8600 2900
	0    1    1    0   
$EndComp
Text GLabel 8900 1150 2    60   Input ~ 0
+5V
Text GLabel 4250 6750 2    60   Input ~ 0
+5V
Text GLabel 4200 5150 2    60   Input ~ 0
+5V
Text GLabel 4200 3850 2    60   Input ~ 0
+5V
Text GLabel 4150 2250 2    60   Input ~ 0
+5V
Text GLabel 4150 850  2    60   Input ~ 0
+5V
$Comp
L SANYOU_SRD_Form_C K1
U 1 1 5AE9995B
P 3850 1050
F 0 "K1" H 4300 1200 50  0000 L CNN
F 1 "SANYOU_SRD_Form_C" H 4300 1100 50  0001 L CNN
F 2 "Relays_THT:Relay_SPDT_OMRON-G5LE-1" H 5300 1000 50  0001 C CNN
F 3 "" H 3850 1050 50  0001 C CNN
	1    3850 1050
	0    1    1    0   
$EndComp
Text GLabel 3550 850  0    60   Input ~ 0
O1
Text GLabel 3550 2250 0    60   Input ~ 0
O2
Text GLabel 3600 3850 0    60   Input ~ 0
O3
Text GLabel 3600 5150 0    60   Input ~ 0
O4
Text GLabel 8300 1150 0    60   Input ~ 0
O6
Text GLabel 8300 2700 0    60   Input ~ 0
O7
Text GLabel 2450 1200 2    60   Input ~ 0
K1_3
Text GLabel 2450 1100 2    60   Input ~ 0
K1_1
Text GLabel 2450 1000 2    60   Input ~ 0
K1_4
Text GLabel 2450 2550 2    60   Input ~ 0
K2_3
Text GLabel 2450 2450 2    60   Input ~ 0
K2_1
Text GLabel 2450 2350 2    60   Input ~ 0
K2_4
Text GLabel 2500 4200 2    60   Input ~ 0
K3_3
Text GLabel 2500 4100 2    60   Input ~ 0
K3_1
Text GLabel 2500 4000 2    60   Input ~ 0
K3_4
Text GLabel 2500 5450 2    60   Input ~ 0
K4_3
Text GLabel 2500 5350 2    60   Input ~ 0
K4_1
Text GLabel 2500 5250 2    60   Input ~ 0
K4_4
Text GLabel 3550 1250 0    60   Input ~ 0
K1_1
Text GLabel 4150 1150 2    60   Input ~ 0
K1_3
Text GLabel 4150 1350 2    60   Input ~ 0
K1_4
Text GLabel 3550 2650 0    60   Input ~ 0
K2_1
Text GLabel 4150 2550 2    60   Input ~ 0
K2_3
Text GLabel 4150 2750 2    60   Input ~ 0
K2_4
Text GLabel 3600 4250 0    60   Input ~ 0
K3_1
Text GLabel 4200 4150 2    60   Input ~ 0
K3_3
Text GLabel 4200 4350 2    60   Input ~ 0
K3_4
Text GLabel 3600 5550 0    60   Input ~ 0
K4_1
Text GLabel 4200 5650 2    60   Input ~ 0
K4_4
Text GLabel 4200 5450 2    60   Input ~ 0
K4_3
Text GLabel 3650 7150 0    60   Input ~ 0
K5_1
Text GLabel 4250 7050 2    60   Input ~ 0
K5_3
Text GLabel 4250 7250 2    60   Input ~ 0
K5_4
Text GLabel 8300 1550 0    60   Input ~ 0
K6_1
Text GLabel 8900 1450 2    60   Input ~ 0
K6_3
Text GLabel 8900 1650 2    60   Input ~ 0
K6_4
Text GLabel 2500 7000 2    60   Input ~ 0
K5_1
Text GLabel 2500 6900 2    60   Input ~ 0
K5_4
Text GLabel 2500 7100 2    60   Input ~ 0
K5_3
Text GLabel 7600 1500 2    60   Input ~ 0
K6_1
Text GLabel 7600 1400 2    60   Input ~ 0
K6_4
Text GLabel 7600 1600 2    60   Input ~ 0
K6_3
Text GLabel 7600 2750 2    60   Input ~ 0
K7_4
Text GLabel 7600 2850 2    60   Input ~ 0
K7_1
Text GLabel 7600 2950 2    60   Input ~ 0
K7_3
Text GLabel 8300 3100 0    60   Input ~ 0
K7_1
Text GLabel 8900 3000 2    60   Input ~ 0
K7_3
Text GLabel 8900 3200 2    60   Input ~ 0
K7_4
$Comp
L C C1
U 1 1 5AE9D944
P 7850 4500
F 0 "C1" H 7875 4600 50  0000 L CNN
F 1 "C" H 7875 4400 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7888 4350 50  0001 C CNN
F 3 "" H 7850 4500 50  0001 C CNN
	1    7850 4500
	0    1    1    0   
$EndComp
Text GLabel 3650 6750 0    60   Input ~ 0
O5
$Comp
L ULN2003A U1
U 1 1 5AE99FB5
P 8050 5400
F 0 "U1" H 8050 5925 50  0000 C CNN
F 1 "ULN2003A" H 8050 5850 50  0000 C CNN
F 2 "SMD_Packages:SO-16-N" H 8100 4750 50  0001 L CNN
F 3 "" H 8150 5300 50  0001 C CNN
	1    8050 5400
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5AEBA592
P 10100 3150
F 0 "#PWR01" H 10100 2900 50  0001 C CNN
F 1 "GND" H 10100 3000 50  0000 C CNN
F 2 "" H 10100 3150 50  0001 C CNN
F 3 "" H 10100 3150 50  0001 C CNN
	1    10100 3150
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5AEBA7E8
P 10100 2900
F 0 "#FLG02" H 10100 2975 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 3050 50  0000 C CNN
F 2 "" H 10100 2900 50  0001 C CNN
F 3 "" H 10100 2900 50  0001 C CNN
	1    10100 2900
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5AEBA81E
P 10550 2900
F 0 "#FLG03" H 10550 2975 50  0001 C CNN
F 1 "PWR_FLAG" H 10550 3050 50  0000 C CNN
F 2 "" H 10550 2900 50  0001 C CNN
F 3 "" H 10550 2900 50  0001 C CNN
	1    10550 2900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5AEBA854
P 10550 3150
F 0 "#PWR04" H 10550 3000 50  0001 C CNN
F 1 "+5V" H 10550 3290 50  0000 C CNN
F 2 "" H 10550 3150 50  0001 C CNN
F 3 "" H 10550 3150 50  0001 C CNN
	1    10550 3150
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR05
U 1 1 5AEBB8DF
P 10600 5600
F 0 "#PWR05" H 10600 5350 50  0001 C CNN
F 1 "GND" H 10600 5450 50  0000 C CNN
F 2 "" H 10600 5600 50  0001 C CNN
F 3 "" H 10600 5600 50  0001 C CNN
	1    10600 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5AEBBA39
P 8550 4600
F 0 "#PWR06" H 8550 4350 50  0001 C CNN
F 1 "GND" H 8550 4450 50  0000 C CNN
F 2 "" H 8550 4600 50  0001 C CNN
F 3 "" H 8550 4600 50  0001 C CNN
	1    8550 4600
	1    0    0    -1  
$EndComp
Text GLabel 9500 5150 0    60   Input ~ 0
I5
Text GLabel 9500 5250 0    60   Input ~ 0
I4
Text GLabel 9500 5350 0    60   Input ~ 0
I3
Text GLabel 9500 5450 0    60   Input ~ 0
I2
Text GLabel 9500 5550 0    60   Input ~ 0
I1
Text GLabel 10150 5550 2    60   Input ~ 0
+5V
Text GLabel 10150 5150 2    60   Input ~ 0
I6
Text GLabel 10150 5250 2    60   Input ~ 0
I7
Wire Wire Line
	7650 4500 7700 4500
Wire Wire Line
	7650 4500 7650 4900
Wire Wire Line
	7650 4700 7450 4700
Connection ~ 7650 4700
Wire Wire Line
	9950 5150 10150 5150
Wire Wire Line
	9950 5250 10150 5250
Wire Wire Line
	9950 5350 10600 5350
Wire Wire Line
	9700 5250 9500 5250
Wire Wire Line
	9700 5150 9500 5150
Wire Wire Line
	9700 5550 9500 5550
Wire Notes Line
	6000 7800 6000 450 
Wire Notes Line
	6000 3800 11200 3800
Wire Wire Line
	9950 5550 10150 5550
Wire Wire Line
	9950 5450 10600 5450
Wire Wire Line
	10550 3150 10550 2900
Wire Wire Line
	10100 3150 10100 2900
Wire Wire Line
	8050 4700 8050 4500
Wire Wire Line
	8000 4500 8550 4500
Wire Wire Line
	8550 4500 8550 4600
Connection ~ 8050 4500
Wire Wire Line
	9700 5350 9500 5350
Wire Wire Line
	9700 5450 9500 5450
Wire Wire Line
	10600 5350 10600 5600
Connection ~ 10600 5450
$EndSCHEMATC
