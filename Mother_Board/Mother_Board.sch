EESchema Schematic File Version 2
LIBS:Mother_Board-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:IDC_2X5
LIBS:Lora_DRF1276DM
LIBS:MIC29302AWU-TR
LIBS:MSP432_LAUNCHPAD
LIBS:USB-A
LIBS:M95
LIBS:TP4056
LIBS:1N5822
LIBS:1N4007
LIBS:CN3791
LIBS:AO4435
LIBS:Mother_Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Hub Mother Board"
Date "2018-07-11"
Rev "v1.1"
Comp "Soil Agritech Pvt. Ltd."
Comment1 "Designed by SRS"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02 J5
U 1 1 5B35E174
P 6550 6750
F 0 "J5" H 6550 6850 50  0000 C CNN
F 1 "Conn_01x02" H 6550 6550 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 6550 6750 50  0001 C CNN
F 3 "" H 6550 6750 50  0001 C CNN
	1    6550 6750
	1    0    0    -1  
$EndComp
$Comp
L LoRa_DRF1276DM-RESCUE-Mother_Board U4
U 1 1 5B361759
P 750 6850
F 0 "U4" H 1000 6450 60  0000 C CNN
F 1 "LoRa_DRF1276DM" H 900 6300 60  0000 C CNN
F 2 "LoRa_DRF1276DM:LoRa_DRF1276DM" H 900 7050 60  0001 C CNN
F 3 "" H 900 7050 60  0001 C CNN
	1    750  6850
	1    0    0    -1  
$EndComp
$Comp
L MIC29302AWU-TR IC1
U 1 1 5B36179A
P 8700 1900
F 0 "IC1" H 8900 1650 50  0001 L BNN
F 1 "MIC29302AWU-TR" H 8350 2350 50  0001 L BNN
F 2 "TO-263:TO263-5" H 8550 1650 50  0001 L BNN
F 3 "MIC29302AWU-TR" H 8350 2350 50  0001 L BNN
	1    8700 1900
	1    0    0    -1  
$EndComp
$Comp
L IDC CON1
U 1 1 5B3617B8
P 3500 4650
F 0 "CON1" H 3430 4980 50  0000 C CNN
F 1 "IDC" H 3410 4320 50  0000 L BNN
F 2 "IDC_2X5:IDC-Header_2x05" V 3180 4670 50  0001 C CNN
F 3 "" H 3600 4650 50  0001 C CNN
	1    3500 4650
	1    0    0    -1  
$EndComp
$Comp
L Jack-DC J6
U 1 1 5B362E39
P 2800 6850
F 0 "J6" H 2800 7060 50  0000 C CNN
F 1 "Jack-DC" H 2800 6675 50  0000 C CNN
F 2 "DC_JACK:DC_JACK" H 2850 6810 50  0001 C CNN
F 3 "" H 2850 6810 50  0001 C CNN
	1    2800 6850
	1    0    0    -1  
$EndComp
$Comp
L M95 U3
U 1 1 5B372D22
P 10150 4700
F 0 "U3" H 10150 4700 60  0000 C CNN
F 1 "M95" H 10150 5150 60  0000 C CNN
F 2 "M95:M95" H 10000 5050 60  0001 C CNN
F 3 "" H 10000 5050 60  0001 C CNN
	1    10150 4700
	1    0    0    -1  
$EndComp
Text Notes 3300 3550 0    60   ~ 0
Relay IDC 2x5
Text Notes 1400 3450 0    60   ~ 0
LCD 20X4
Text Notes 8600 2300 0    60   ~ 0
LDO
Text Notes 9650 4000 0    60   ~ 0
M95 Breakout
Text Notes 3100 6450 0    60   ~ 0
DC Power Jack \nFrom AC Adaptor
Text Notes 5750 6450 0    60   ~ 0
Phase Detector
$Comp
L Conn_01x02 J2
U 1 1 5B37514F
P 7200 4700
F 0 "J2" H 7300 4700 50  0000 C CNN
F 1 "Conn_01x02" H 7100 4500 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 7200 4700 50  0001 C CNN
F 3 "" H 7200 4700 50  0001 C CNN
	1    7200 4700
	1    0    0    -1  
$EndComp
Text Notes 6850 5100 0    60   ~ 0
Li-ion Cell\nConnector
Text Notes 3100 3100 0    60   ~ 0
MSP432 Launchpad
$Comp
L TP4056 U2
U 1 1 5B37536B
P 5900 4700
F 0 "U2" H 5900 4350 60  0000 C CNN
F 1 "TP4056" H 5900 4950 60  0000 C CNN
F 2 "TP4056:TP4056" H 5750 4850 60  0001 C CNN
F 3 "" H 5750 4850 60  0001 C CNN
	1    5900 4700
	1    0    0    -1  
$EndComp
Text Notes 5600 4300 0    60   ~ 0
  Single Cell\nLi-ion Charger
$Comp
L PWR_FLAG #FLG01
U 1 1 5B37C2C3
P 9600 6200
F 0 "#FLG01" H 9600 6275 50  0001 C CNN
F 1 "PWR_FLAG" H 9600 6350 50  0000 C CNN
F 2 "" H 9600 6200 50  0001 C CNN
F 3 "" H 9600 6200 50  0001 C CNN
	1    9600 6200
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B37C2EC
P 10100 6000
F 0 "#FLG02" H 10100 6075 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 6150 50  0000 C CNN
F 2 "" H 10100 6000 50  0001 C CNN
F 3 "" H 10100 6000 50  0001 C CNN
	1    10100 6000
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 5B37C335
P 9600 6000
F 0 "#PWR03" H 9600 5850 50  0001 C CNN
F 1 "+5V" H 9600 6140 50  0000 C CNN
F 2 "" H 9600 6000 50  0001 C CNN
F 3 "" H 9600 6000 50  0001 C CNN
	1    9600 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5B3A1701
P 4150 5300
F 0 "#PWR04" H 4150 5050 50  0001 C CNN
F 1 "GND" H 4150 5150 50  0000 C CNN
F 2 "" H 4150 5300 50  0001 C CNN
F 3 "" H 4150 5300 50  0001 C CNN
	1    4150 5300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 5B3A1729
P 3800 5000
F 0 "#PWR05" H 3800 4850 50  0001 C CNN
F 1 "+5V" H 3800 5140 50  0000 C CNN
F 2 "" H 3800 5000 50  0001 C CNN
F 3 "" H 3800 5000 50  0001 C CNN
	1    3800 5000
	-1   0    0    1   
$EndComp
Text GLabel 3850 4450 2    60   Input ~ 0
R6_RELAY
Text GLabel 3850 4550 2    60   Input ~ 0
R7_RELAY
Text GLabel 3150 4450 0    60   Input ~ 0
R5_RELAY
Text GLabel 3150 4550 0    60   Input ~ 0
R4_RELAY
Text GLabel 3150 4650 0    60   Input ~ 0
R3_RELAY
Text GLabel 3150 4750 0    60   Input ~ 0
R2_RELAY
Text GLabel 3150 4850 0    60   Input ~ 0
R1_RELAY
$Comp
L GND #PWR06
U 1 1 5B3A1B5F
P 5050 5100
F 0 "#PWR06" H 5050 4850 50  0001 C CNN
F 1 "GND" H 5050 4950 50  0000 C CNN
F 2 "" H 5050 5100 50  0001 C CNN
F 3 "" H 5050 5100 50  0001 C CNN
	1    5050 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5B3A1B88
P 6750 5100
F 0 "#PWR07" H 6750 4850 50  0001 C CNN
F 1 "GND" H 6750 4950 50  0000 C CNN
F 2 "" H 6750 5100 50  0001 C CNN
F 3 "" H 6750 5100 50  0001 C CNN
	1    6750 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5B3A1BEE
P 11100 5300
F 0 "#PWR08" H 11100 5050 50  0001 C CNN
F 1 "GND" H 11100 5150 50  0000 C CNN
F 2 "" H 11100 5300 50  0001 C CNN
F 3 "" H 11100 5300 50  0001 C CNN
	1    11100 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5B3A21D0
P 3850 7300
F 0 "#PWR09" H 3850 7050 50  0001 C CNN
F 1 "GND" H 3850 7150 50  0000 C CNN
F 2 "" H 3850 7300 50  0001 C CNN
F 3 "" H 3850 7300 50  0001 C CNN
	1    3850 7300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B3A2D8E
P 1550 7450
F 0 "#PWR010" H 1550 7200 50  0001 C CNN
F 1 "GND" H 1550 7300 50  0000 C CNN
F 2 "" H 1550 7450 50  0001 C CNN
F 3 "" H 1550 7450 50  0001 C CNN
	1    1550 7450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5B3A2DBE
P 7700 2800
F 0 "#PWR011" H 7700 2550 50  0001 C CNN
F 1 "GND" H 7700 2650 50  0000 C CNN
F 2 "" H 7700 2800 50  0001 C CNN
F 3 "" H 7700 2800 50  0001 C CNN
	1    7700 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5B3A3584
P 3400 1150
F 0 "#PWR012" H 3400 900 50  0001 C CNN
F 1 "GND" H 3400 1000 50  0000 C CNN
F 2 "" H 3400 1150 50  0001 C CNN
F 3 "" H 3400 1150 50  0001 C CNN
	1    3400 1150
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR013
U 1 1 5B3A36F7
P 5550 1950
F 0 "#PWR013" H 5550 1700 50  0001 C CNN
F 1 "GND" H 5550 1800 50  0000 C CNN
F 2 "" H 5550 1950 50  0001 C CNN
F 3 "" H 5550 1950 50  0001 C CNN
	1    5550 1950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR014
U 1 1 5B3A4845
P 4400 6600
F 0 "#PWR014" H 4400 6450 50  0001 C CNN
F 1 "+5V" H 4400 6740 50  0000 C CNN
F 2 "" H 4400 6600 50  0001 C CNN
F 3 "" H 4400 6600 50  0001 C CNN
	1    4400 6600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5B3A513E
P 6050 7200
F 0 "#PWR015" H 6050 6950 50  0001 C CNN
F 1 "GND" H 6050 7050 50  0000 C CNN
F 2 "" H 6050 7200 50  0001 C CNN
F 3 "" H 6050 7200 50  0001 C CNN
	1    6050 7200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5B3A55BB
P 1250 5550
F 0 "#PWR016" H 1250 5300 50  0001 C CNN
F 1 "GND" H 1250 5400 50  0000 C CNN
F 2 "" H 1250 5550 50  0001 C CNN
F 3 "" H 1250 5550 50  0001 C CNN
	1    1250 5550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5B3A55EB
P 1250 4500
F 0 "#PWR017" H 1250 4250 50  0001 C CNN
F 1 "GND" H 1250 4350 50  0000 C CNN
F 2 "" H 1250 4500 50  0001 C CNN
F 3 "" H 1250 4500 50  0001 C CNN
	1    1250 4500
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 5B3A747B
P 1350 4000
F 0 "#PWR018" H 1350 3850 50  0001 C CNN
F 1 "+5V" H 1350 4140 50  0000 C CNN
F 2 "" H 1350 4000 50  0001 C CNN
F 3 "" H 1350 4000 50  0001 C CNN
	1    1350 4000
	1    0    0    -1  
$EndComp
Text GLabel 1800 4400 0    60   Input ~ 0
RS_LCD
Text GLabel 1800 4600 0    60   Input ~ 0
E_LCD
Text GLabel 1800 4700 0    60   Input ~ 0
D4_LCD
Text GLabel 1800 4800 0    60   Input ~ 0
D5_LCD
Text GLabel 1800 4900 0    60   Input ~ 0
D6_LCD
Text GLabel 1800 5000 0    60   Input ~ 0
D7_LCD
$Comp
L +5V #PWR019
U 1 1 5B3A765C
P 1250 4950
F 0 "#PWR019" H 1250 4800 50  0001 C CNN
F 1 "+5V" H 1250 5090 50  0000 C CNN
F 2 "" H 1250 4950 50  0001 C CNN
F 3 "" H 1250 4950 50  0001 C CNN
	1    1250 4950
	1    0    0    -1  
$EndComp
NoConn ~ 9400 4550
$Comp
L +5V #PWR020
U 1 1 5B3A8BCB
P 7300 1650
F 0 "#PWR020" H 7300 1500 50  0001 C CNN
F 1 "+5V" H 7300 1790 50  0000 C CNN
F 2 "" H 7300 1650 50  0001 C CNN
F 3 "" H 7300 1650 50  0001 C CNN
	1    7300 1650
	1    0    0    -1  
$EndComp
$Comp
L CP1 C4
U 1 1 5B3A8C07
P 7400 2250
F 0 "C4" H 7425 2350 50  0000 L CNN
F 1 "470u" H 7425 2150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 7400 2250 50  0001 C CNN
F 3 "" H 7400 2250 50  0001 C CNN
	1    7400 2250
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5B3A8C4E
P 7700 2250
F 0 "C5" H 7725 2350 50  0000 L CNN
F 1 "100n" H 7725 2150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 7738 2100 50  0001 C CNN
F 3 "" H 7700 2250 50  0001 C CNN
	1    7700 2250
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B3A8C7D
P 9550 2000
F 0 "R1" V 9630 2000 50  0000 C CNN
F 1 "120k" V 9550 2000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 2000 50  0001 C CNN
F 3 "" H 9550 2000 50  0001 C CNN
	1    9550 2000
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5B3A8CEB
P 9550 2450
F 0 "R3" V 9630 2450 50  0000 C CNN
F 1 "51k" V 9550 2450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 2450 50  0001 C CNN
F 3 "" H 9550 2450 50  0001 C CNN
	1    9550 2450
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B3A8DEB
P 9950 2200
F 0 "R2" V 10030 2200 50  0000 C CNN
F 1 "470E" V 9950 2200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9880 2200 50  0001 C CNN
F 3 "" H 9950 2200 50  0001 C CNN
	1    9950 2200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C2
U 1 1 5B3A8ECE
P 10250 2200
F 0 "C2" H 10275 2300 50  0000 L CNN
F 1 "470u" H 10275 2100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 10250 2200 50  0001 C CNN
F 3 "" H 10250 2200 50  0001 C CNN
	1    10250 2200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5B3A8ED4
P 10550 2200
F 0 "C3" H 10575 2300 50  0000 L CNN
F 1 "100n" H 10575 2100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 10588 2050 50  0001 C CNN
F 3 "" H 10550 2200 50  0001 C CNN
	1    10550 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5B3A8F4F
P 10400 2800
F 0 "#PWR021" H 10400 2550 50  0001 C CNN
F 1 "GND" H 10400 2650 50  0000 C CNN
F 2 "" H 10400 2800 50  0001 C CNN
F 3 "" H 10400 2800 50  0001 C CNN
	1    10400 2800
	1    0    0    -1  
$EndComp
Text GLabel 10950 1800 2    60   Input ~ 0
VBAT
Text Notes 8400 1300 0    60   ~ 0
M95 Power Supply
Text GLabel 9150 4350 0    60   Input ~ 0
VBAT
$Comp
L 1N5822 D3
U 1 1 5B3A7C45
P 3850 6750
F 0 "D3" H 3650 6850 50  0000 L BNN
F 1 "1N5822" H 3800 6850 50  0000 L BNN
F 2 "1N5822:DIOAD2120W130L950D530P" H 3850 6750 50  0001 L BNN
F 3 "Unavailable" H 3850 6750 50  0001 L BNN
F 4 "DO-201AD-201 CDIL" H 3850 6750 50  0001 L BNN "Field5"
F 5 "STMicroelectronics" H 3850 6750 50  0001 L BNN "Field6"
F 6 "None" H 3850 6750 50  0001 L BNN "Field7"
F 7 "1N5822" H 3850 6750 50  0001 L BNN "Field8"
	1    3850 6750
	1    0    0    -1  
$EndComp
$Comp
L 1N5822 D2
U 1 1 5B3A804A
P 7200 4450
F 0 "D2" H 7050 4519 50  0000 L BNN
F 1 "1N5822" H 7050 4309 50  0000 L BNN
F 2 "1N5822:DIOAD2120W130L950D530P" H 7200 4450 50  0001 L BNN
F 3 "Unavailable" H 7200 4450 50  0001 L BNN
F 4 "DO-201AD-201 CDIL" H 7200 4450 50  0001 L BNN "Field5"
F 5 "STMicroelectronics" H 7200 4450 50  0001 L BNN "Field6"
F 6 "None" H 7200 4450 50  0001 L BNN "Field7"
F 7 "1N5822" H 7200 4450 50  0001 L BNN "Field8"
	1    7200 4450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR022
U 1 1 5B3A810D
P 5050 4300
F 0 "#PWR022" H 5050 4150 50  0001 C CNN
F 1 "+5V" H 5050 4440 50  0000 C CNN
F 2 "" H 5050 4300 50  0001 C CNN
F 3 "" H 5050 4300 50  0001 C CNN
	1    5050 4300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR023
U 1 1 5B3A82D7
P 7600 4300
F 0 "#PWR023" H 7600 4150 50  0001 C CNN
F 1 "+5V" H 7750 4350 50  0000 C CNN
F 2 "" H 7600 4300 50  0001 C CNN
F 3 "" H 7600 4300 50  0001 C CNN
	1    7600 4300
	1    0    0    -1  
$EndComp
Text GLabel 5800 6750 0    60   Input ~ 0
Phase_Detector
Text GLabel 4850 2700 2    60   Input ~ 0
Phase_Detector
Text GLabel 4850 2600 2    60   Input ~ 0
R1_RELAY
Text GLabel 4850 2500 2    60   Input ~ 0
R2_RELAY
Text GLabel 4850 2400 2    60   Input ~ 0
R3_RELAY
Text GLabel 4850 2300 2    60   Input ~ 0
R4_RELAY
Text GLabel 4850 2100 2    60   Input ~ 0
R5_RELAY
Text GLabel 4850 2000 2    60   Input ~ 0
R6_RELAY
Text GLabel 4850 1900 2    60   Input ~ 0
R7_RELAY
$Comp
L Conn_01x02 J4
U 1 1 5B3B2808
P 2350 6750
F 0 "J4" H 2350 6850 50  0000 C CNN
F 1 "Conn_01x02" H 2200 6550 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 2350 6750 50  0001 C CNN
F 3 "" H 2350 6750 50  0001 C CNN
	1    2350 6750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 5B3B2DA0
P 2350 7100
F 0 "#PWR024" H 2350 6950 50  0001 C CNN
F 1 "+5V" H 2350 7240 50  0000 C CNN
F 2 "" H 2350 7100 50  0001 C CNN
F 3 "" H 2350 7100 50  0001 C CNN
	1    2350 7100
	-1   0    0    1   
$EndComp
Text GLabel 9400 4250 0    60   Input ~ 0
NET_M95
Text GLabel 9400 4850 0    60   Input ~ 0
STAT_M95
Text GLabel 9400 4950 0    60   Input ~ 0
RI_M95
Text GLabel 9400 5050 0    60   Input ~ 0
DTR_M95
Text GLabel 9400 5150 0    60   Input ~ 0
PWRKEY_M95
Text GLabel 3950 2200 0    60   Input ~ 0
NET_M95
Text GLabel 3950 2100 0    60   Input ~ 0
STAT_M95
Text GLabel 3950 2000 0    60   Input ~ 0
RI_M95
Text GLabel 3950 1900 0    60   Input ~ 0
DTR_M95
Text GLabel 3950 1800 0    60   Input ~ 0
PWRKEY_M95
NoConn ~ 4850 2200
$Comp
L R R4
U 1 1 5B3B5568
P 1550 5100
F 0 "R4" V 1630 5100 50  0000 C CNN
F 1 "220" V 1550 5100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1480 5100 50  0001 C CNN
F 3 "" H 1550 5100 50  0001 C CNN
	1    1550 5100
	0    1    1    0   
$EndComp
Text GLabel 3950 2500 0    60   Input ~ 0
RS_LCD
Text GLabel 3950 2600 0    60   Input ~ 0
E_LCD
Text GLabel 3000 2500 2    60   Input ~ 0
D4_LCD
Text GLabel 3000 2400 2    60   Input ~ 0
D5_LCD
Text GLabel 3000 2100 2    60   Input ~ 0
D6_LCD
NoConn ~ 3000 2700
NoConn ~ 3000 2600
NoConn ~ 3000 2200
NoConn ~ 3000 2300
Text GLabel 3000 2000 2    60   Input ~ 0
D7_LCD
$Comp
L Conn_01x02 J3
U 1 1 5B3B8199
P 1750 6550
F 0 "J3" H 1750 6650 50  0000 C CNN
F 1 "Conn_01x02" H 1700 6350 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 1750 6550 50  0001 C CNN
F 3 "" H 1750 6550 50  0001 C CNN
	1    1750 6550
	1    0    0    -1  
$EndComp
NoConn ~ 2100 1800
NoConn ~ 2100 2200
NoConn ~ 2100 2300
NoConn ~ 2100 2400
NoConn ~ 2100 2500
NoConn ~ 2100 2600
NoConn ~ 2100 2700
NoConn ~ 3950 2400
$Comp
L +5V #PWR025
U 1 1 5B3B8FED
P 3100 1150
F 0 "#PWR025" H 3100 1000 50  0001 C CNN
F 1 "+5V" H 3100 1290 50  0000 C CNN
F 2 "" H 3100 1150 50  0001 C CNN
F 3 "" H 3100 1150 50  0001 C CNN
	1    3100 1150
	1    0    0    -1  
$EndComp
Text Notes 5700 900  0    118  ~ 24
HUB Mother Board
Text Notes 8300 6050 0    59   ~ 0
Power Flags
$Comp
L C C17
U 1 1 5B3BD008
P 1800 7250
F 0 "C17" H 1825 7350 50  0000 L CNN
F 1 "100n" H 1825 7150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1838 7100 50  0001 C CNN
F 3 "" H 1800 7250 50  0001 C CNN
	1    1800 7250
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 5B3BD317
P 3950 5050
F 0 "C10" H 3975 5150 50  0000 L CNN
F 1 "100n" H 3975 4950 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 3988 4900 50  0001 C CNN
F 3 "" H 3950 5050 50  0001 C CNN
	1    3950 5050
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 5B3BD41F
P 1050 5300
F 0 "C11" H 1075 5400 50  0000 L CNN
F 1 "100n" H 1075 5200 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1088 5150 50  0001 C CNN
F 3 "" H 1050 5300 50  0001 C CNN
	1    1050 5300
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5B3BD528
P 1050 4250
F 0 "C6" H 1075 4350 50  0000 L CNN
F 1 "100n" H 1075 4150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1088 4100 50  0001 C CNN
F 3 "" H 1050 4250 50  0001 C CNN
	1    1050 4250
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B3BD694
P 3250 1650
F 0 "C1" H 3275 1750 50  0000 L CNN
F 1 "100n" H 3275 1550 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 3288 1500 50  0001 C CNN
F 3 "" H 3250 1650 50  0001 C CNN
	1    3250 1650
	0    -1   -1   0   
$EndComp
$Comp
L C C9
U 1 1 5B3BD966
P 5050 4750
F 0 "C9" H 5075 4850 50  0000 L CNN
F 1 "100n" H 5075 4650 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 5088 4600 50  0001 C CNN
F 3 "" H 5050 4750 50  0001 C CNN
	1    5050 4750
	1    0    0    -1  
$EndComp
$Comp
L CP1 C8
U 1 1 5B3BDBAA
P 4850 4750
F 0 "C8" H 4875 4850 50  0000 L CNN
F 1 "470u" H 4875 4650 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 4850 4750 50  0001 C CNN
F 3 "" H 4850 4750 50  0001 C CNN
	1    4850 4750
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 5B3BDCFA
P 7600 4800
F 0 "C7" H 7625 4900 50  0000 L CNN
F 1 "100n" H 7625 4700 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 7638 4650 50  0001 C CNN
F 3 "" H 7600 4800 50  0001 C CNN
	1    7600 4800
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 5B3BE3EB
P 4350 7000
F 0 "C15" H 4375 7100 50  0000 L CNN
F 1 "100n" H 4375 6900 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 4388 6850 50  0001 C CNN
F 3 "" H 4350 7000 50  0001 C CNN
	1    4350 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 C14
U 1 1 5B3BE3F1
P 4150 7000
F 0 "C14" H 4175 7100 50  0000 L CNN
F 1 "470u" H 4175 6900 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 4150 7000 50  0001 C CNN
F 3 "" H 4150 7000 50  0001 C CNN
	1    4150 7000
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 5B3BEB50
P 3600 7000
F 0 "C13" H 3625 7100 50  0000 L CNN
F 1 "100n" H 3625 6900 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 3638 6850 50  0001 C CNN
F 3 "" H 3600 7000 50  0001 C CNN
	1    3600 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 C12
U 1 1 5B3BEB56
P 3400 7000
F 0 "C12" H 3425 7100 50  0000 L CNN
F 1 "470u" H 3425 6900 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 3400 7000 50  0001 C CNN
F 3 "" H 3400 7000 50  0001 C CNN
	1    3400 7000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 5B3BF9B7
P 1800 7450
F 0 "#PWR026" H 1800 7200 50  0001 C CNN
F 1 "GND" H 1800 7300 50  0000 C CNN
F 2 "" H 1800 7450 50  0001 C CNN
F 3 "" H 1800 7450 50  0001 C CNN
	1    1800 7450
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 5B3BFFDD
P 5900 6950
F 0 "C16" H 5925 7050 50  0000 L CNN
F 1 "100n" H 5925 6850 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 5938 6800 50  0001 C CNN
F 3 "" H 5900 6950 50  0001 C CNN
	1    5900 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 5B3C2AF9
P 7600 5100
F 0 "#PWR027" H 7600 4850 50  0001 C CNN
F 1 "GND" H 7600 4950 50  0000 C CNN
F 2 "" H 7600 5100 50  0001 C CNN
F 3 "" H 7600 5100 50  0001 C CNN
	1    7600 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR028
U 1 1 5B3CE45D
P 10100 6200
F 0 "#PWR028" H 10100 5950 50  0001 C CNN
F 1 "GND" H 10100 6050 50  0000 C CNN
F 2 "" H 10100 6200 50  0001 C CNN
F 3 "" H 10100 6200 50  0001 C CNN
	1    10100 6200
	1    0    0    -1  
$EndComp
Text Notes 10650 2800 0    59   ~ 0
1N4733
$Comp
L 1N4007 D1
U 1 1 5B3B3C64
P 10900 2250
F 0 "D1" V 11050 2050 50  0000 L BNN
F 1 "1N4007" H 10699 2049 50  0001 L BNN
F 2 "1N4007:DIOAD1060W80L520D270" H 10900 2250 50  0001 L BNN
F 3 "Plc Splitter 1x32 s-Grade 3m" H 10900 2250 50  0001 L BNN
F 4 "Unavailable" H 10900 2250 50  0001 L BNN "Field4"
F 5 "1N4007" H 10900 2250 50  0001 L BNN "Field5"
F 6 "0.01 USD" H 10900 2250 50  0001 L BNN "Field6"
F 7 "DO-41 Microsemi" H 10900 2250 50  0001 L BNN "Field7"
F 8 "Micro Commercial" H 10900 2250 50  0001 L BNN "Field8"
	1    10900 2250
	0    -1   -1   0   
$EndComp
Text Notes 10650 3000 0    60   ~ 0
5.1V, 1W\nZener Diode
$Comp
L GND #PWR029
U 1 1 5B3A3C14
P 9100 4500
F 0 "#PWR029" H 9100 4250 50  0001 C CNN
F 1 "GND" H 8950 4450 50  0000 C CNN
F 2 "" H 9100 4500 50  0001 C CNN
F 3 "" H 9100 4500 50  0001 C CNN
	1    9100 4500
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5B3C8248
P 6750 4150
F 0 "R6" V 6830 4150 50  0000 C CNN
F 1 "47k" V 6750 4150 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6680 4150 50  0001 C CNN
F 3 "" H 6750 4150 50  0001 C CNN
	1    6750 4150
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5B3C8350
P 6750 3700
F 0 "R5" V 6830 3700 50  0000 C CNN
F 1 "68k" V 6750 3700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6680 3700 50  0001 C CNN
F 3 "" H 6750 3700 50  0001 C CNN
	1    6750 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR030
U 1 1 5B3C83B2
P 5950 3600
F 0 "#PWR030" H 5950 3350 50  0001 C CNN
F 1 "GND" H 5950 3450 50  0000 C CNN
F 2 "" H 5950 3600 50  0001 C CNN
F 3 "" H 5950 3600 50  0001 C CNN
	1    5950 3600
	1    0    0    -1  
$EndComp
$Comp
L CP1 C20
U 1 1 5B3C9EDA
P 4300 5050
F 0 "C20" H 4325 5150 50  0000 L CNN
F 1 "470u" H 4325 4950 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 4300 5050 50  0001 C CNN
F 3 "" H 4300 5050 50  0001 C CNN
	1    4300 5050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C21
U 1 1 5B3CA231
P 800 5300
F 0 "C21" H 825 5400 50  0000 L CNN
F 1 "470u" H 825 5200 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 800 5300 50  0001 C CNN
F 3 "" H 800 5300 50  0001 C CNN
	1    800  5300
	1    0    0    -1  
$EndComp
$Comp
L CP1 C19
U 1 1 5B3CA2F2
P 800 4250
F 0 "C19" H 825 4350 50  0000 L CNN
F 1 "470u" H 825 4150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 800 4250 50  0001 C CNN
F 3 "" H 800 4250 50  0001 C CNN
	1    800  4250
	1    0    0    -1  
$EndComp
$Comp
L CP1 C22
U 1 1 5B3CA980
P 2150 7250
F 0 "C22" H 2175 7350 50  0000 L CNN
F 1 "470u" H 2175 7150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 2150 7250 50  0001 C CNN
F 3 "" H 2150 7250 50  0001 C CNN
	1    2150 7250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5B3CABD8
P 2150 7450
F 0 "#PWR031" H 2150 7200 50  0001 C CNN
F 1 "GND" H 2150 7300 50  0000 C CNN
F 2 "" H 2150 7450 50  0001 C CNN
F 3 "" H 2150 7450 50  0001 C CNN
	1    2150 7450
	1    0    0    -1  
$EndComp
$Comp
L 1N5822 D4
U 1 1 5B3CB20A
P 7200 4150
F 0 "D4" H 7050 4219 50  0000 L BNN
F 1 "1N5822" H 7050 4009 50  0000 L BNN
F 2 "1N5822:DIOAD2120W130L950D530P" H 7200 4150 50  0001 L BNN
F 3 "Unavailable" H 7200 4150 50  0001 L BNN
F 4 "DO-201AD-201 CDIL" H 7200 4150 50  0001 L BNN "Field5"
F 5 "STMicroelectronics" H 7200 4150 50  0001 L BNN "Field6"
F 6 "None" H 7200 4150 50  0001 L BNN "Field7"
F 7 "1N5822" H 7200 4150 50  0001 L BNN "Field8"
	1    7200 4150
	1    0    0    -1  
$EndComp
Text GLabel 7300 4150 2    60   Input ~ 0
VBAT
$Comp
L GND #PWR032
U 1 1 5B3CC3A7
P 4150 4700
F 0 "#PWR032" H 4150 4450 50  0001 C CNN
F 1 "GND" H 4300 4650 50  0000 C CNN
F 2 "" H 4150 4700 50  0001 C CNN
F 3 "" H 4150 4700 50  0001 C CNN
	1    4150 4700
	1    0    0    -1  
$EndComp
$Comp
L CP1 C18
U 1 1 5B3CDFC1
P 3250 1400
F 0 "C18" H 3275 1500 50  0000 L CNN
F 1 "470u" H 3275 1300 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D10.0mm_P5.00mm" H 3250 1400 50  0001 C CNN
F 3 "" H 3250 1400 50  0001 C CNN
	1    3250 1400
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x02 J7
U 1 1 5B3E0E1A
P 6250 3950
F 0 "J7" H 6250 4050 50  0000 C CNN
F 1 "Conn_01x02" H 6250 3750 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 6250 3950 50  0001 C CNN
F 3 "" H 6250 3950 50  0001 C CNN
	1    6250 3950
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02 J1
U 1 1 5B3E1626
P 1150 1900
F 0 "J1" H 1150 2000 50  0000 C CNN
F 1 "Conn_01x02" H 1150 1700 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 1150 1900 50  0001 C CNN
F 3 "" H 1150 1900 50  0001 C CNN
	1    1150 1900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR033
U 1 1 5B3E1C29
P 1400 1550
F 0 "#PWR033" H 1400 1300 50  0001 C CNN
F 1 "GND" H 1400 1400 50  0000 C CNN
F 2 "" H 1400 1550 50  0001 C CNN
F 3 "" H 1400 1550 50  0001 C CNN
	1    1400 1550
	-1   0    0    1   
$EndComp
NoConn ~ 10900 5150
$Comp
L Conn_01x02 J9
U 1 1 5B3E3DDA
P 1150 2100
F 0 "J9" H 1150 2200 50  0000 C CNN
F 1 "Conn_01x02" H 1100 2300 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 1150 2100 50  0001 C CNN
F 3 "" H 1150 2100 50  0001 C CNN
	1    1150 2100
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02 J10
U 1 1 5B3E48FE
P 8100 4650
F 0 "J10" H 8100 4750 50  0000 C CNN
F 1 "Conn_01x02" H 8000 4400 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B02B-EH-A_02x2.50mm_Straight" H 8100 4650 50  0001 C CNN
F 3 "" H 8100 4650 50  0001 C CNN
	1    8100 4650
	-1   0    0    -1  
$EndComp
$Comp
L MSP432_LAUNCHPAD U1
U 1 1 5B362882
P 3500 2250
F 0 "U1" H 5200 1600 60  0000 C CNN
F 1 "MSP432_LAUNCHPAD" H 2150 2950 60  0000 C CNN
F 2 "MSP432_LAUNCHPAD:msp430" H 2050 2100 60  0001 C CNN
F 3 "" H 2050 2100 60  0001 C CNN
	1    3500 2250
	1    0    0    -1  
$EndComp
Text Notes 8700 4600 0    60   ~ 0
RX
Text Notes 5600 3900 0    60   ~ 0
Cell Voltage
Text Notes 1400 2200 0    60   ~ 0
RX
Text Notes 1400 2000 0    60   ~ 0
TX
Text Notes 1050 1650 0    60   ~ 0
Cell\nVoltage
$Comp
L C C26
U 1 1 5B45AF85
P 6550 4150
F 0 "C26" H 6575 4250 50  0000 L CNN
F 1 "100n" H 6575 4050 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 6588 4000 50  0001 C CNN
F 3 "" H 6550 4150 50  0001 C CNN
	1    6550 4150
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 5B45BF73
P 1300 6300
F 0 "C29" H 1325 6400 50  0000 L CNN
F 1 "100n" H 1325 6200 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1338 6150 50  0001 C CNN
F 3 "" H 1300 6300 50  0001 C CNN
	1    1300 6300
	1    0    0    -1  
$EndComp
$Comp
L C C30
U 1 1 5B45C19F
P 1500 6300
F 0 "C30" H 1525 6400 50  0000 L CNN
F 1 "100n" H 1525 6200 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1538 6150 50  0001 C CNN
F 3 "" H 1500 6300 50  0001 C CNN
	1    1500 6300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR034
U 1 1 5B45CC4E
P 750 6150
F 0 "#PWR034" H 750 5900 50  0001 C CNN
F 1 "GND" H 750 6000 50  0000 C CNN
F 2 "" H 750 6150 50  0001 C CNN
F 3 "" H 750 6150 50  0001 C CNN
	1    750  6150
	1    0    0    -1  
$EndComp
$Comp
L C C23
U 1 1 5B45E861
P 1550 1150
F 0 "C23" H 1575 1250 50  0000 L CNN
F 1 "100n" H 1575 1050 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1588 1000 50  0001 C CNN
F 3 "" H 1550 1150 50  0001 C CNN
	1    1550 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 5B45EC99
P 1550 900
F 0 "#PWR035" H 1550 650 50  0001 C CNN
F 1 "GND" H 1550 750 50  0000 C CNN
F 2 "" H 1550 900 50  0001 C CNN
F 3 "" H 1550 900 50  0001 C CNN
	1    1550 900 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4450 3850 4450
Wire Wire Line
	3650 4550 3850 4550
Wire Wire Line
	3400 4450 3150 4450
Wire Wire Line
	3400 4550 3150 4550
Wire Wire Line
	3400 4650 3150 4650
Wire Wire Line
	3400 4750 3150 4750
Wire Wire Line
	3400 4850 3150 4850
Wire Wire Line
	4150 5250 4150 5300
Wire Wire Line
	3650 4650 4150 4650
Wire Wire Line
	3650 4750 3950 4750
Wire Wire Line
	3950 4750 3950 4650
Connection ~ 3950 4650
Wire Wire Line
	6750 4800 6750 5100
Wire Wire Line
	4850 5000 5150 5000
Wire Wire Line
	5050 4900 5050 5100
Wire Wire Line
	11100 5300 11100 5050
Wire Wire Line
	11100 5050 10900 5050
Wire Wire Line
	3100 6850 3200 6850
Wire Wire Line
	3100 6950 3200 6950
Wire Wire Line
	3200 6850 3200 7250
Wire Wire Line
	6750 5000 6650 5000
Wire Wire Line
	6650 4850 6750 4850
Wire Wire Line
	6750 4800 7000 4800
Wire Wire Line
	7950 2000 8150 2000
Wire Wire Line
	1550 6950 1550 7450
Wire Wire Line
	1550 7150 1250 7150
Wire Wire Line
	3400 1900 3000 1900
Wire Wire Line
	5550 1800 5550 1950
Wire Wire Line
	4850 1800 5550 1800
Wire Wire Line
	3100 6750 3750 6750
Wire Wire Line
	6200 7100 6200 6850
Wire Wire Line
	6200 6850 6350 6850
Wire Wire Line
	1250 4100 1900 4100
Wire Wire Line
	1250 4100 1250 4500
Wire Wire Line
	1250 5200 1900 5200
Wire Wire Line
	1250 5200 1250 5550
Wire Wire Line
	1350 4200 1900 4200
Wire Wire Line
	1350 4000 1350 4200
Wire Wire Line
	1500 4300 1900 4300
Wire Wire Line
	1500 4300 1500 4100
Connection ~ 1500 4100
Wire Wire Line
	1600 4500 1900 4500
Wire Wire Line
	1600 4500 1600 4300
Connection ~ 1600 4300
Wire Wire Line
	1250 5100 1250 4950
Wire Wire Line
	7300 1800 8150 1800
Wire Wire Line
	7950 1800 7950 1900
Wire Wire Line
	7950 1900 8150 1900
Wire Wire Line
	7300 1800 7300 1650
Connection ~ 7950 1800
Wire Wire Line
	7400 2100 7400 1800
Connection ~ 7400 1800
Wire Wire Line
	7700 2100 7700 1800
Connection ~ 7700 1800
Wire Wire Line
	7400 2400 7400 2700
Wire Wire Line
	7400 2700 7950 2700
Wire Wire Line
	7700 2400 7700 2800
Connection ~ 7700 2700
Wire Wire Line
	9250 1800 10950 1800
Wire Wire Line
	10550 2050 10550 1800
Connection ~ 10550 1800
Wire Wire Line
	10250 2050 10250 1800
Connection ~ 10250 1800
Wire Wire Line
	9550 1850 9550 1800
Connection ~ 9550 1800
Wire Wire Line
	9250 1900 9250 2250
Wire Wire Line
	9250 2250 9550 2250
Wire Wire Line
	9550 2150 9550 2300
Connection ~ 9550 2250
Wire Wire Line
	9950 2050 9950 1800
Connection ~ 9950 1800
Wire Wire Line
	9550 2600 9550 2700
Wire Wire Line
	9550 2700 10900 2700
Wire Wire Line
	10400 2700 10400 2800
Connection ~ 10400 2700
Wire Wire Line
	10550 2350 10550 2700
Connection ~ 10550 2700
Wire Wire Line
	10250 2350 10250 2700
Connection ~ 10250 2700
Wire Wire Line
	9950 2350 9950 2700
Connection ~ 9950 2700
Connection ~ 10900 1800
Wire Wire Line
	9400 4350 9150 4350
Wire Wire Line
	5050 4300 5050 4600
Wire Wire Line
	4850 4450 5150 4450
Wire Wire Line
	6650 4450 7100 4450
Wire Wire Line
	7300 4450 7600 4450
Wire Wire Line
	7600 4300 7600 4650
Wire Wire Line
	3950 6750 4400 6750
Wire Wire Line
	4400 6750 4400 6600
Wire Wire Line
	1250 6750 2150 6750
Wire Wire Line
	1250 6850 2150 6850
Wire Wire Line
	1250 6950 1550 6950
Connection ~ 1550 7150
Wire Wire Line
	1250 7050 2350 7050
Wire Wire Line
	1700 5100 1900 5100
Wire Wire Line
	800  5100 1400 5100
Wire Notes Line
	450  3200 11250 3200
Wire Notes Line
	11250 3200 11250 3250
Wire Notes Line
	7900 3200 7900 6550
Wire Notes Line
	4650 3200 4650 7800
Wire Notes Line
	2500 3200 2500 7800
Wire Notes Line
	450  6000 7900 6000
Wire Notes Line
	5750 3200 5750 500 
Wire Wire Line
	7950 2700 7950 2000
Wire Wire Line
	8150 2100 7950 2100
Connection ~ 7950 2100
Wire Wire Line
	3600 6850 3600 6750
Connection ~ 3600 6750
Wire Wire Line
	3400 6850 3400 6750
Connection ~ 3400 6750
Wire Wire Line
	4150 6850 4150 6750
Connection ~ 4150 6750
Wire Wire Line
	4350 6850 4350 6750
Connection ~ 4350 6750
Wire Wire Line
	3400 7150 3400 7250
Wire Wire Line
	3200 7250 4350 7250
Wire Wire Line
	4350 7250 4350 7150
Wire Wire Line
	4150 7150 4150 7250
Connection ~ 4150 7250
Wire Wire Line
	3600 7150 3600 7250
Connection ~ 3600 7250
Wire Wire Line
	3850 7300 3850 7250
Connection ~ 3850 7250
Wire Wire Line
	1800 7100 1800 7050
Connection ~ 1800 7050
Wire Wire Line
	1800 7450 1800 7400
Wire Wire Line
	5900 7100 6200 7100
Wire Wire Line
	1050 5450 1050 5500
Wire Wire Line
	800  5500 1250 5500
Connection ~ 1250 5500
Wire Wire Line
	1050 5150 1050 5100
Connection ~ 1250 5100
Wire Wire Line
	1050 4100 1050 4050
Wire Wire Line
	800  4050 1350 4050
Connection ~ 1350 4050
Wire Wire Line
	1050 4400 1050 4450
Wire Wire Line
	800  4450 1250 4450
Connection ~ 1250 4450
Wire Wire Line
	3100 1800 3000 1800
Wire Wire Line
	3100 1150 3100 1800
Wire Wire Line
	3400 1150 3400 1900
Wire Wire Line
	3800 5000 3800 4850
Wire Wire Line
	3650 4850 4300 4850
Wire Wire Line
	3950 4850 3950 4900
Connection ~ 3800 4850
Wire Wire Line
	3950 5200 3950 5250
Wire Wire Line
	3950 5250 4300 5250
Connection ~ 4150 5250
Wire Wire Line
	4850 4600 4850 4450
Connection ~ 5050 4450
Connection ~ 5050 5000
Wire Wire Line
	4850 4900 4850 5000
Connection ~ 7600 4450
Wire Wire Line
	7600 5100 7600 4950
Wire Wire Line
	9600 6200 9600 6000
Wire Wire Line
	6050 7200 6050 7100
Connection ~ 6050 7100
Wire Wire Line
	5800 6750 6350 6750
Wire Wire Line
	5900 6800 5900 6750
Connection ~ 5900 6750
Wire Wire Line
	10100 6200 10100 6000
Wire Wire Line
	10900 1800 10900 2050
Wire Wire Line
	10900 2700 10900 2450
Wire Wire Line
	9400 4450 9100 4450
Wire Wire Line
	9100 4450 9100 4500
Wire Wire Line
	6750 3500 6750 3550
Wire Wire Line
	5950 3500 6750 3500
Wire Wire Line
	5950 3500 5950 3600
Wire Wire Line
	6750 3850 6750 4000
Wire Wire Line
	6750 4300 6750 4450
Connection ~ 6750 4450
Wire Wire Line
	4300 5250 4300 5200
Wire Wire Line
	4300 4850 4300 4900
Connection ~ 3950 4850
Wire Wire Line
	800  4100 800  4050
Connection ~ 1050 4050
Wire Wire Line
	800  4450 800  4400
Connection ~ 1050 4450
Wire Wire Line
	800  5150 800  5100
Connection ~ 1050 5100
Wire Wire Line
	800  5500 800  5450
Connection ~ 1050 5500
Wire Wire Line
	2150 7450 2150 7400
Wire Wire Line
	6950 4450 6950 4150
Wire Wire Line
	6950 4150 7100 4150
Connection ~ 6950 4450
Wire Wire Line
	4150 4650 4150 4700
Wire Wire Line
	2150 7050 2150 7100
Connection ~ 2150 7050
Connection ~ 3100 1650
Connection ~ 3100 1400
Connection ~ 3400 1650
Connection ~ 3400 1400
Connection ~ 6750 4850
Connection ~ 6750 5000
Wire Wire Line
	6650 4600 6750 4600
Wire Wire Line
	6750 4600 6750 4700
Wire Wire Line
	6750 4700 7000 4700
Wire Wire Line
	6450 3850 6550 3850
Wire Wire Line
	6550 3850 6550 3500
Connection ~ 6550 3500
Wire Wire Line
	1350 1900 2100 1900
Wire Wire Line
	1400 1800 1350 1800
Wire Wire Line
	1800 4400 1900 4400
Wire Wire Line
	1800 4600 1900 4600
Wire Wire Line
	1800 4700 1900 4700
Wire Wire Line
	1800 4800 1900 4800
Wire Wire Line
	1800 4900 1900 4900
Wire Wire Line
	1800 5000 1900 5000
Wire Wire Line
	1350 2100 2100 2100
Wire Wire Line
	8300 4750 9400 4750
Wire Wire Line
	8300 4650 9400 4650
Wire Wire Line
	6450 3950 6750 3950
Connection ~ 6750 3950
Connection ~ 3400 7250
Connection ~ 3200 6950
Wire Wire Line
	6550 4000 6550 3950
Connection ~ 6550 3950
Wire Wire Line
	6550 4300 6650 4300
Wire Wire Line
	6650 4300 6650 4350
Wire Wire Line
	6650 4350 6750 4350
Connection ~ 6750 4350
Wire Wire Line
	1250 6550 1550 6550
Wire Wire Line
	1250 6650 1550 6650
Wire Wire Line
	2350 7050 2350 7100
Wire Wire Line
	750  6150 750  6100
Wire Wire Line
	1500 6100 1500 6150
Connection ~ 1500 6100
Wire Wire Line
	1300 6150 1300 6100
Connection ~ 1300 6100
Wire Wire Line
	1300 6450 1300 6550
Connection ~ 1300 6550
Wire Wire Line
	1400 1800 1400 1550
Wire Wire Line
	1550 1300 1550 1900
Connection ~ 1550 1900
Wire Wire Line
	1550 900  1550 1000
Wire Wire Line
	1500 6450 1500 6650
Connection ~ 1500 6650
$Comp
L Conn_01x06 J8
U 1 1 5B60849D
P 2100 4300
F 0 "J8" H 2100 4600 50  0000 C CNN
F 1 "Conn_01x06" H 2100 4700 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B06B-EH-A_06x2.50mm_Straight" H 2100 4300 50  0001 C CNN
F 3 "" H 2100 4300 50  0001 C CNN
	1    2100 4300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06 J11
U 1 1 5B608B57
P 2100 4900
F 0 "J11" H 2100 4400 50  0000 C CNN
F 1 "Conn_01x06" H 2100 4500 50  0000 C CNN
F 2 "Connectors_JST:JST_EH_B06B-EH-A_06x2.50mm_Straight" H 2100 4900 50  0001 C CNN
F 3 "" H 2100 4900 50  0001 C CNN
	1    2100 4900
	1    0    0    -1  
$EndComp
Wire Notes Line
	7200 3200 7200 450 
$Comp
L Conn_01x04 J12
U 1 1 5B60B945
P 6650 1900
F 0 "J12" H 6650 2100 50  0000 C CNN
F 1 "Conn_01x04" H 6650 1600 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-4_P5.08mm" H 6650 1900 50  0001 C CNN
F 3 "" H 6650 1900 50  0001 C CNN
	1    6650 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 5B60BA60
P 6150 2200
F 0 "#PWR036" H 6150 1950 50  0001 C CNN
F 1 "GND" H 6150 2050 50  0000 C CNN
F 2 "" H 6150 2200 50  0001 C CNN
F 3 "" H 6150 2200 50  0001 C CNN
	1    6150 2200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR037
U 1 1 5B60BC4E
P 6150 1650
F 0 "#PWR037" H 6150 1500 50  0001 C CNN
F 1 "+5V" H 6150 1790 50  0000 C CNN
F 2 "" H 6150 1650 50  0001 C CNN
F 3 "" H 6150 1650 50  0001 C CNN
	1    6150 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1650 6150 1800
Wire Wire Line
	6150 1800 6450 1800
Wire Wire Line
	6450 2100 6150 2100
Wire Wire Line
	6150 2100 6150 2200
Text GLabel 6450 1900 0    60   Input ~ 0
TRIG
Text GLabel 6450 2000 0    60   Input ~ 0
ECO
Text GLabel 3950 2700 0    60   Input ~ 0
TRIG
Text GLabel 3950 2300 0    60   Input ~ 0
ECO
Wire Wire Line
	1350 2000 2100 2000
Wire Wire Line
	750  6100 1500 6100
$EndSCHEMATC
