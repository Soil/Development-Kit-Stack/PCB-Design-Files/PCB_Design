PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SON-6
$EndINDEX
$MODULE SON-6
Po 0 0 0 15 00000000 00000000 ~~
Li SON-6
Cd 4-Channel ESD Protection Array for High-Speed Data Interfaces 6-SON -40 to 85
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0 0 1 0.9 0 0.05 N H 21 "SON-6"
T1 0 0 1 0.9 0 0.05 N H 21 "VAL**"
DS 2.2 -0.525 2.2 -0.475 0 24
DS 0 0 1.5 0 0 24
DS 1.5 0 1.5 -1.05 0 24
DS 1.5 -1.05 0 -1.05 0 24
DS 0 -1.05 0 0 0 24
T2 0.000000 0.636527 1 1 0 .05 N V 51 "1"
$PAD
Sh "IO1" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -0.135
$EndPAD
$PAD
Sh "IO2" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -0.135
$EndPAD
$PAD
Sh "GND" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -0.135
$EndPAD
$PAD
Sh "VCC" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -0.915
$EndPAD
$PAD
Sh "IO4" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -0.915
$EndPAD
$PAD
Sh "IO3" R 0.52 0.3 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -0.915
$EndPAD
$EndMODULE SON-6
