PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
XCVR_M95A-02-NCH-STD
$EndINDEX
$MODULE XCVR_M95A-02-NCH-STD
Po 0 0 0 15 00000000 00000000 ~~
Li XCVR_M95A-02-NCH-STD
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -6.47101 -14.1591 1.0017 1.0017 0 0.05 N V 21 "XCVR_M95A-02-NCH-STD"
T1 -6.83221 14.3802 1.00106 1.00106 0 0.05 N V 21 "VAL**"
DS -8.35 -11.8 -7 -11.8 0.2 24
DS -2.05 -11.8 1 -11.8 0.2 24
DS 7 -11.8 9.95 -11.8 0.2 24
DS 9.95 -11.8 9.95 -8.2 0.2 24
DS 9.95 -2.35 9.95 1.2 0.2 24
DS 9.95 8.1 9.95 11.8 0.2 24
DS 9.95 11.8 6.8 11.8 0.2 24
DS 0.95 11.8 -2.45 11.8 0.2 24
DS -7.05 11.8 -9.95 11.8 0.2 24
DS -9.95 11.8 -9.95 8.1 0.2 24
DS -9.95 -8.2 -9.95 -10.2 0.2 24
DS -9.95 -10.2 -8.35 -11.8 0.2 24
DS -9.95 -2.35 -9.95 1.2 0.2 24
DC -11.95 -7.5 -11.6146 -7.5 0 21
DS -8.75 -13.05 11.2 -13.05 0.127 21
DS 11.2 -13.05 11.2 13.05 0.127 21
DS 11.2 13.05 -11.2 13.05 0.127 21
DS -11.2 13.05 -11.2 -10.6 0.127 21
DS -11.2 -10.6 -8.75 -13.05 0.127 21
DS -14 -16 14 -16 0.05 26
DS 14 -16 14 16 0.05 26
DS 14 16 -14 16 0.05 26
DS -14 16 -14 -16 0.05 26
$PAD
Sh "1" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 -7.5
$EndPAD
$PAD
Sh "2" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 -6.4
$EndPAD
$PAD
Sh "3" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 -5.3
$EndPAD
$PAD
Sh "4" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 -4.2
$EndPAD
$PAD
Sh "5" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 -3.1
$EndPAD
$PAD
Sh "6" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 1.9
$EndPAD
$PAD
Sh "7" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 3
$EndPAD
$PAD
Sh "8" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 4.1
$EndPAD
$PAD
Sh "9" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 5.2
$EndPAD
$PAD
Sh "10" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 6.3
$EndPAD
$PAD
Sh "11" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.8 7.4
$EndPAD
$PAD
Sh "12" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.35 11.65
$EndPAD
$PAD
Sh "13" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.25 11.65
$EndPAD
$PAD
Sh "14" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.15 11.65
$EndPAD
$PAD
Sh "15" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.05 11.65
$EndPAD
$PAD
Sh "16" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.65 11.65
$EndPAD
$PAD
Sh "17" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.75 11.65
$EndPAD
$PAD
Sh "18" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.85 11.65
$EndPAD
$PAD
Sh "19" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.95 11.65
$EndPAD
$PAD
Sh "20" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.05 11.65
$EndPAD
$PAD
Sh "21" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 7.4
$EndPAD
$PAD
Sh "22" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 6.3
$EndPAD
$PAD
Sh "23" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 5.2
$EndPAD
$PAD
Sh "24" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 4.1
$EndPAD
$PAD
Sh "25" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 3
$EndPAD
$PAD
Sh "26" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 1.9
$EndPAD
$PAD
Sh "27" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 -3.1
$EndPAD
$PAD
Sh "28" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 -4.2
$EndPAD
$PAD
Sh "29" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 -5.3
$EndPAD
$PAD
Sh "30" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 -6.4
$EndPAD
$PAD
Sh "31" R 0.75 2.4 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.8 -7.5
$EndPAD
$PAD
Sh "32" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.05 -11.65
$EndPAD
$PAD
Sh "33" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.95 -11.65
$EndPAD
$PAD
Sh "34" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.85 -11.65
$EndPAD
$PAD
Sh "35" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.75 -11.65
$EndPAD
$PAD
Sh "36" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.65 -11.65
$EndPAD
$PAD
Sh "37" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.05 -11.65
$EndPAD
$PAD
Sh "38" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.15 -11.65
$EndPAD
$PAD
Sh "39" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.25 -11.65
$EndPAD
$PAD
Sh "40" R 0.75 2.4 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.35 -11.65
$EndPAD
$EndMODULE XCVR_M95A-02-NCH-STD
