PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
DIL16-A
DIL16-B
DIL16-E
SO16W
SO16-B
SO16
$EndINDEX
$MODULE DIL16-A
Po 0 0 0 15 00000000 00000000 ~~
Li DIL16-A
Cd <b>Dual In Line Package</b>
Sc 00000000
At STD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 2.99197 1.4005 0.701752 0.701752 0 0.05 N V 21 "DIL16-A"
T1 1.71996 -0.955532 0.501592 0.501592 0 0.05 N V 21 "VAL**"
DS 10.16 -2.921 -10.16 -2.921 0.1524 24
DS -10.16 2.921 10.16 2.921 0.1524 24
DS 10.16 -2.921 10.16 2.921 0.1524 24
DS -10.16 -2.921 -10.16 -1.016 0.1524 24
DS -10.16 2.921 -10.16 1.016 0.1524 24
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
DS 10.16 -2.921 -10.16 -2.921 0.1524 24
DS -10.16 2.921 10.16 2.921 0.1524 24
DS 10.16 -2.921 10.16 2.921 0.1524 24
DS -10.16 -2.921 -10.16 -1.016 0.1524 24
DS -10.16 2.921 -10.16 1.016 0.1524 24
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
DS 10.16 -2.921 -10.16 -2.921 0.1524 24
DS -10.16 2.921 10.16 2.921 0.1524 24
DS 10.16 -2.921 10.16 2.921 0.1524 24
DS -10.16 -2.921 -10.16 -1.016 0.1524 24
DS -10.16 2.921 -10.16 1.016 0.1524 24
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
DS 10.16 -2.921 -10.16 -2.921 0.1524 24
DS -10.16 2.921 10.16 2.921 0.1524 24
DS 10.16 -2.921 10.16 2.921 0.1524 24
DS -10.16 -2.921 -10.16 -1.016 0.1524 24
DS -10.16 2.921 -10.16 1.016 0.1524 24
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
$PAD
Sh "1" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 3.81
$EndPAD
$PAD
Sh "2" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 3.81
$EndPAD
$PAD
Sh "7" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 3.81
$EndPAD
$PAD
Sh "8" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 3.81
$EndPAD
$PAD
Sh "3" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 3.81
$EndPAD
$PAD
Sh "4" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 3.81
$EndPAD
$PAD
Sh "6" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 3.81
$EndPAD
$PAD
Sh "5" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 3.81
$EndPAD
$PAD
Sh "9" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 -3.81
$EndPAD
$PAD
Sh "10" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 -3.81
$EndPAD
$PAD
Sh "11" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 -3.81
$EndPAD
$PAD
Sh "12" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 -3.81
$EndPAD
$PAD
Sh "13" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 -3.81
$EndPAD
$PAD
Sh "14" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 -3.81
$EndPAD
$PAD
Sh "15" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 -3.81
$EndPAD
$PAD
Sh "16" C 1.308 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 -3.81
$EndPAD
$EndMODULE DIL16-A
$MODULE DIL16-B
Po 0 0 0 15 00000000 00000000 ~~
Li DIL16-B
Cd 
Sc 00000000
At STD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -11.2516 2.66986 0.70075 0.70075 900 0.05 N V 21 "DIL16-B"
T1 11.1525 -2.8678 0.501803 0.501803 900 0.05 N V 21 "VAL**"
DA -10.16 0 -10.16 1.27 -1800 0.127 21
DA -10.16 0 -10.16 1.27 -1800 0.127 21
DS -10.16 -1.27 -10.16 -5.715 0.127 21
DS -10.16 -5.715 10.16 -5.715 0.127 21
DS 10.16 -5.715 10.16 5.715 0.127 21
DS 10.16 5.715 -10.16 5.715 0.127 21
DS -10.16 5.715 -10.16 1.27 0.127 21
$PAD
Sh "1" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 3.81
$EndPAD
$PAD
Sh "2" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 3.81
$EndPAD
$PAD
Sh "3" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 3.81
$EndPAD
$PAD
Sh "4" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 3.81
$EndPAD
$PAD
Sh "5" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 3.81
$EndPAD
$PAD
Sh "6" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 3.81
$EndPAD
$PAD
Sh "7" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 3.81
$EndPAD
$PAD
Sh "8" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 3.81
$EndPAD
$PAD
Sh "9" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 -3.81
$EndPAD
$PAD
Sh "10" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 -3.81
$EndPAD
$PAD
Sh "11" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 -3.81
$EndPAD
$PAD
Sh "12" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 -3.81
$EndPAD
$PAD
Sh "13" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 -3.81
$EndPAD
$PAD
Sh "14" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 -3.81
$EndPAD
$PAD
Sh "15" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 -3.81
$EndPAD
$PAD
Sh "16" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 -3.81
$EndPAD
$EndMODULE DIL16-B
$MODULE DIL16-E
Po 0 0 0 15 00000000 00000000 ~~
Li DIL16-E
Cd 
Sc 00000000
At STD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -3.68758 1.08084 0.700871 0.700871 0 0.05 N V 21 "DIL16-E"
T1 -2.73548 -1.27232 0.500912 0.500912 0 0.05 N V 21 "VAL**"
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
DS 10.16 -2.921 -10.16 -2.921 0.1524 24
DS -10.16 2.921 10.16 2.921 0.1524 24
DS 10.16 -2.921 10.16 2.921 0.1524 24
DS -10.16 -2.921 -10.16 -1.016 0.1524 24
DS -10.16 2.921 -10.16 1.016 0.1524 24
DA -10.16 0 -10.16 1.016 -1800 0.1524 21
$PAD
Sh "1" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 3.81
$EndPAD
$PAD
Sh "2" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 3.81
$EndPAD
$PAD
Sh "7" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 3.81
$EndPAD
$PAD
Sh "8" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 3.81
$EndPAD
$PAD
Sh "3" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 3.81
$EndPAD
$PAD
Sh "4" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 3.81
$EndPAD
$PAD
Sh "6" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 3.81
$EndPAD
$PAD
Sh "5" O 2.616 1.308 0 0 2700
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 3.81
$EndPAD
$PAD
Sh "9" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 8.89 -3.81
$EndPAD
$PAD
Sh "10" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 6.35 -3.81
$EndPAD
$PAD
Sh "11" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 3.81 -3.81
$EndPAD
$PAD
Sh "12" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 1.27 -3.81
$EndPAD
$PAD
Sh "13" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 -3.81
$EndPAD
$PAD
Sh "14" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -3.81 -3.81
$EndPAD
$PAD
Sh "15" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -6.35 -3.81
$EndPAD
$PAD
Sh "16" O 2.616 1.308 0 0 900
Dr 0.8 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -8.89 -3.81
$EndPAD
$EndMODULE DIL16-E
$MODULE SO16W
Po 0 0 0 15 00000000 00000000 ~~
Li SO16W
Cd <b>Small Outline Package</b> 7.6 x 10.6 mm
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -6.18339 3.3148 0.702715 0.702715 900 0.05 N V 21 "SO16W"
T1 5.80245 -1.51023 0.500696 0.500696 900 0.05 N V 21 "VAL**"
DS 5.19 5.7637 -5.19 5.7637 0.2032 21
DS -5.19 2.9062 -5.19 2.88255 0.2032 21
DS -4.1275 -0.5398 -4.3815 -0.7938 0.2032 21
DS -4.3815 -0.7938 -4.9848 -0.7938 0.2032 21
DS -4.9848 -0.7938 -5.2388 -1.0478 0.2032 21
DS -5.2388 -1.0478 -5.2388 -5.7637 0.2032 21
DS -5.2388 -5.7637 5.19 -5.7637 0.2032 21
DS 5.19 2.88255 -5.19 2.88255 0.2032 21
DS 5.19 -5.7637 5.19 2.88255 0.2032 21
DS 5.19 2.88255 5.19 5.7637 0.2032 21
DS -4.1275 -0.5398 -4.1275 0.5398 0.2032 21
DS -4.1275 0.5398 -4.3815 0.7938 0.2032 21
DS -4.3815 0.7938 -4.9848 0.7938 0.2032 21
DS -4.9848 0.7938 -5.2388 1.0478 0.2032 21
DS -5.2388 1.0478 -5.2388 5.7637 0.2032 21
DP 0 0 0 0 4 0 24
Dl -4.70097 3.8
Dl -4.2 3.8
Dl -4.2 5.33244
Dl -4.70097 5.33244
DP 0 0 0 0 4 0 24
Dl -3.42909 3.8
Dl -2.93 3.8
Dl -2.93 5.33414
Dl -3.42909 5.33414
DP 0 0 0 0 4 0 24
Dl -2.15469 3.8
Dl -1.66 3.8
Dl -1.66 5.33158
Dl -2.15469 5.33158
DP 0 0 0 0 4 0 24
Dl -0.882378 3.8
Dl -0.39 3.8
Dl -0.39 5.33438
Dl -0.882378 5.33438
DP 0 0 0 0 4 0 24
Dl 0.391412 3.8
Dl 0.88 3.8
Dl 0.88 5.33927
Dl 0.391412 5.33927
DP 0 0 0 0 4 0 24
Dl 1.6633 3.8
Dl 2.15 3.8
Dl 2.15 5.33059
Dl 1.6633 5.33059
DP 0 0 0 0 4 0 24
Dl 2.93719 3.8
Dl 3.42 3.8
Dl 3.42 5.33307
Dl 2.93719 5.33307
DP 0 0 0 0 4 0 24
Dl 4.20545 3.8
Dl 4.69 3.8
Dl 4.69 5.32691
Dl 4.20545 5.32691
DP 0 0 0 0 4 0 24
Dl 4.20746 -5.32
Dl 4.69 -5.32
Dl 4.69 -3.80675
Dl 4.20746 -3.80675
DP 0 0 0 0 4 0 24
Dl 2.93498 -5.32
Dl 3.42 -5.32
Dl 3.42 -3.80646
Dl 2.93498 -3.80646
DP 0 0 0 0 4 0 24
Dl 1.66244 -5.32
Dl 2.15 -5.32
Dl 2.15 -3.80558
Dl 1.66244 -3.80558
DP 0 0 0 0 4 0 24
Dl 0.390909 -5.32
Dl 0.88 -5.32
Dl 0.88 -3.80886
Dl 0.390909 -3.80886
DP 0 0 0 0 4 0 24
Dl -0.881497 -5.32
Dl -0.39 -5.32
Dl -0.39 -3.80646
Dl -0.881497 -3.80646
DP 0 0 0 0 4 0 24
Dl -2.15398 -5.32
Dl -1.66 -5.32
Dl -1.66 -3.80703
Dl -2.15398 -3.80703
DP 0 0 0 0 4 0 24
Dl -3.42614 -5.32
Dl -2.93 -5.32
Dl -2.93 -3.80682
Dl -3.42614 -3.80682
DP 0 0 0 0 4 0 24
Dl -4.7008 -5.32
Dl -4.2 -5.32
Dl -4.2 -3.80875
Dl -4.7008 -3.80875
$PAD
Sh "2" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 4.6
$EndPAD
$PAD
Sh "13" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 -4.6
$EndPAD
$PAD
Sh "1" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 4.6
$EndPAD
$PAD
Sh "3" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 4.6
$EndPAD
$PAD
Sh "4" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 4.6
$EndPAD
$PAD
Sh "14" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 -4.6
$EndPAD
$PAD
Sh "12" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 -4.6
$EndPAD
$PAD
Sh "11" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 -4.6
$EndPAD
$PAD
Sh "6" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 4.6
$EndPAD
$PAD
Sh "9" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 -4.6
$EndPAD
$PAD
Sh "5" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 4.6
$EndPAD
$PAD
Sh "7" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 4.6
$EndPAD
$PAD
Sh "10" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 -4.6
$EndPAD
$PAD
Sh "8" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 4.6
$EndPAD
$PAD
Sh "15" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 -4.6
$EndPAD
$PAD
Sh "16" R 1.6764 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 -4.6
$EndPAD
$EndMODULE SO16W
$MODULE SO16-B
Po 0 0 0 15 00000000 00000000 ~~
Li SO16-B
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -5.8487 -0.190717 0.700802 0.700802 900 0.05 N V 21 "SO16-B"
T1 5.88376 -1.66971 0.500852 0.500852 900 0.05 N V 21 "VAL**"
DS -5.08 -0.8255 -5.08 -0.79375 0.1524 21
DS -5.08 -0.79375 -5.08 0.15875 0.1524 21
DS -5.08 0.15875 -5.08 0.1905 0.1524 21
DA -5.08 -0.3175 -5.08 0.1905 -1800 0.1524 21
DS -5.08 1.2827 5.23875 1.2827 0.0508 21
DP 0 0 0 0 4 0 24
Dl -0.891882 -3.0988
Dl -0.381 -3.0988
Dl -0.381 -1.96215
Dl -0.891882 -1.96215
DP 0 0 0 0 4 0 24
Dl -4.70855 1.9558
Dl -4.191 1.9558
Dl -4.191 3.1051
Dl -4.70855 3.1051
DP 0 0 0 0 4 0 24
Dl -3.43568 1.9558
Dl -2.921 1.9558
Dl -2.921 3.10483
Dl -3.43568 3.10483
DP 0 0 0 0 4 0 24
Dl -2.1647 1.9304
Dl -1.651 1.9304
Dl -1.651 3.08152
Dl -2.1647 3.08152
DP 0 0 0 0 4 0 24
Dl -0.891611 1.9558
Dl -0.381 1.9558
Dl -0.381 3.1079
Dl -0.891611 3.1079
DP 0 0 0 0 4 0 24
Dl -2.16393 -3.0988
Dl -1.651 -3.0988
Dl -1.651 -1.96027
Dl -2.16393 -1.96027
DP 0 0 0 0 4 0 24
Dl -3.43132 -3.0988
Dl -2.921 -3.0988
Dl -2.921 -1.95712
Dl -3.43132 -1.95712
DP 0 0 0 0 4 0 24
Dl -4.71095 -3.0988
Dl -4.191 -3.0988
Dl -4.191 -1.96077
Dl -4.71095 -1.96077
DP 0 0 0 0 4 0 24
Dl 0.38152 1.9558
Dl 0.889 1.9558
Dl 0.889 3.10302
Dl 0.38152 3.10302
DP 0 0 0 0 4 0 24
Dl 1.65618 1.9558
Dl 2.159 1.9558
Dl 2.159 3.10851
Dl 1.65618 3.10851
DP 0 0 0 0 4 0 24
Dl 2.92218 1.9558
Dl 3.429 1.9558
Dl 3.429 3.10005
Dl 2.92218 3.10005
DP 0 0 0 0 4 0 24
Dl 4.19715 1.9558
Dl 4.699 1.9558
Dl 4.699 3.10335
Dl 4.19715 3.10335
DP 0 0 0 0 4 0 24
Dl 0.382039 -3.0988
Dl 0.889 -3.0988
Dl 0.889 -1.96113
Dl 0.382039 -1.96113
DP 0 0 0 0 4 0 24
Dl 1.65515 -3.0988
Dl 2.159 -3.0988
Dl 2.159 -1.96072
Dl 1.65515 -1.96072
DP 0 0 0 0 4 0 24
Dl 2.92632 -3.0988
Dl 3.429 -3.0988
Dl 3.429 -1.95936
Dl 2.92632 -1.95936
DP 0 0 0 0 4 0 24
Dl 4.1997 -3.0988
Dl 4.699 -3.0988
Dl 4.699 -1.95986
Dl 4.1997 -1.95986
DS -5.08 -0.8255 -5.08 -6.0325 0.1524 21
DS -5.08 -6.0325 5.23875 -6.0325 0.1524 21
DS 5.23875 -6.0325 5.23875 1.2827 0.1524 21
DS 5.23875 1.2827 5.23875 5.55625 0.1524 21
DS 5.23875 5.55625 -5.08 5.55625 0.1524 21
DS -5.08 5.55625 -5.08 1.2827 0.1524 21
DS -5.08 1.2827 -5.08 0.1905 0.1524 21
$PAD
Sh "2" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 3.5
$EndPAD
$PAD
Sh "13" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 -3.9
$EndPAD
$PAD
Sh "1" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 3.5
$EndPAD
$PAD
Sh "3" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 3.5
$EndPAD
$PAD
Sh "4" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 3.5
$EndPAD
$PAD
Sh "14" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 -3.9
$EndPAD
$PAD
Sh "12" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 -3.9
$EndPAD
$PAD
Sh "11" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 -3.9
$EndPAD
$PAD
Sh "6" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 3.5
$EndPAD
$PAD
Sh "9" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 -3.9
$EndPAD
$PAD
Sh "5" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 3.5
$EndPAD
$PAD
Sh "7" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 3.5
$EndPAD
$PAD
Sh "10" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 -3.9
$EndPAD
$PAD
Sh "8" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 3.5
$EndPAD
$PAD
Sh "15" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 -3.9
$EndPAD
$PAD
Sh "16" R 3.5 0.8128 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 -3.9
$EndPAD
$EndMODULE SO16-B
$MODULE SO16
Po 0 0 0 15 00000000 00000000 ~~
Li SO16
Cd <b>Small Outline Package</b>
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -5.91846 2.00105e-06 0.701536 0.701536 900 0.05 N V 21 "SO16"
T1 -2.48079 0.31805 0.500867 0.500867 0 0.05 N V 21 "VAL**"
DS 4.699 -4.33705 -4.699 -4.33705 0.1524 21
DA 4.699 4.1148 5.08 4.1148 900 0.1524 21
DA -4.699 -3.95605 -4.699 -4.33705 -900 0.1524 21
DA 4.699 -3.95605 5.08 -3.95605 -900 0.1524 21
DA -4.699 4.1148 -4.699 4.4958 900 0.1524 21
DS -4.699 4.4958 4.699 4.4958 0.1524 21
DS 5.08 4.1148 5.08 -3.95605 0.1524 21
DS -5.08 -3.95605 -5.08 4.1148 0.1524 21
DA -5.08 0 -5.08 0.508 -1800 0.1524 21
DS -5.08 1.6002 5.08 1.6002 0.0508 21
DP 0 0 0 0 4 0 24
Dl -0.891427 -3.0988
Dl -0.381 -3.0988
Dl -0.381 -1.96114
Dl -0.891427 -1.96114
DP 0 0 0 0 4 0 24
Dl -4.70944 1.9558
Dl -4.191 1.9558
Dl -4.191 3.10568
Dl -4.70944 3.10568
DP 0 0 0 0 4 0 24
Dl -3.44026 1.9558
Dl -2.921 1.9558
Dl -2.921 3.10897
Dl -3.44026 3.10897
DP 0 0 0 0 4 0 24
Dl -2.16084 1.9304
Dl -1.651 1.9304
Dl -1.651 3.07603
Dl -2.16084 3.07603
DP 0 0 0 0 4 0 24
Dl -0.890167 1.9558
Dl -0.381 1.9558
Dl -0.381 3.10287
Dl -0.890167 3.10287
DP 0 0 0 0 4 0 24
Dl -2.15963 -3.0988
Dl -1.651 -3.0988
Dl -1.651 -1.95638
Dl -2.15963 -1.95638
DP 0 0 0 0 4 0 24
Dl -3.44042 -3.0988
Dl -2.921 -3.0988
Dl -2.921 -1.96231
Dl -3.44042 -1.96231
DP 0 0 0 0 4 0 24
Dl -4.70366 -3.0988
Dl -4.191 -3.0988
Dl -4.191 -1.95774
Dl -4.70366 -1.95774
DP 0 0 0 0 4 0 24
Dl 0.381738 1.9558
Dl 0.889 1.9558
Dl 0.889 3.1048
Dl 0.381738 3.1048
DP 0 0 0 0 4 0 24
Dl 1.65373 1.9558
Dl 2.159 1.9558
Dl 2.159 3.10394
Dl 1.65373 3.10394
DP 0 0 0 0 4 0 24
Dl 2.92323 1.9558
Dl 3.429 1.9558
Dl 3.429 3.10117
Dl 2.92323 3.10117
DP 0 0 0 0 4 0 24
Dl 4.1949 1.9558
Dl 4.699 1.9558
Dl 4.699 3.10169
Dl 4.1949 3.10169
DP 0 0 0 0 4 0 24
Dl 0.381923 -3.0988
Dl 0.889 -3.0988
Dl 0.889 -1.96053
Dl 0.381923 -1.96053
DP 0 0 0 0 4 0 24
Dl 1.65341 -3.0988
Dl 2.159 -3.0988
Dl 2.159 -1.95866
Dl 1.65341 -1.95866
DP 0 0 0 0 4 0 24
Dl 2.92456 -3.0988
Dl 3.429 -3.0988
Dl 3.429 -1.95818
Dl 2.92456 -1.95818
DP 0 0 0 0 4 0 24
Dl 4.19852 -3.0988
Dl 4.699 -3.0988
Dl 4.699 -1.95931
Dl 4.19852 -1.95931
$PAD
Sh "1" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 3.0734
$EndPAD
$PAD
Sh "16" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.445 -3.0734
$EndPAD
$PAD
Sh "2" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 3.0734
$EndPAD
$PAD
Sh "3" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 3.0734
$EndPAD
$PAD
Sh "15" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.175 -3.0734
$EndPAD
$PAD
Sh "14" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.905 -3.0734
$EndPAD
$PAD
Sh "4" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 3.0734
$EndPAD
$PAD
Sh "13" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.635 -3.0734
$EndPAD
$PAD
Sh "5" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 3.0734
$EndPAD
$PAD
Sh "12" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.635 -3.0734
$EndPAD
$PAD
Sh "6" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 3.0734
$EndPAD
$PAD
Sh "7" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 3.0734
$EndPAD
$PAD
Sh "11" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.905 -3.0734
$EndPAD
$PAD
Sh "10" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.175 -3.0734
$EndPAD
$PAD
Sh "8" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 3.0734
$EndPAD
$PAD
Sh "9" R 0.6604 2.032 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.445 -3.0734
$EndPAD
$EndMODULE SO16
