PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
DIOAD2120W130L950D530P
$EndINDEX
$MODULE DIOAD2120W130L950D530P
Po 0 0 0 15 00000000 00000000 ~~
Li DIOAD2120W130L950D530P
Cd 
Sc 00000000
At STD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -2.46324 -3.63977 1.00131 1.00131 0 0.05 N V 21 "DIOAD2120W130L950D530P"
T1 -1.828 4.37217 1.00164 1.00164 0 0.05 N V 21 "VAL**"
DS -4.75 -2.65 4.75 -2.65 0.2 21
DS 4.75 -2.65 4.75 2.65 0.2 21
DS 4.75 2.65 -4.75 2.65 0.2 21
DS -4.75 2.65 -4.75 -2.65 0.2 21
DS -8.89 0 -4.8 0 0.2 21
DS 8.89 0 4.8 0 0.2 21
DS -5 -2.9 5 -2.9 0.05 26
DS 12 -1.35 12 1.35 0.05 26
DS 5 2.9 -5 2.9 0.05 26
DS -12 1.35 -12 -1.35 0.05 26
DS -12 -1.35 -5 -1.35 0.05 26
DS -5 -1.35 -5 -2.9 0.05 26
DS -12 1.35 -5 1.35 0.05 26
DS -5 1.35 -5 2.9 0.05 26
DS 12 -1.35 5 -1.35 0.05 26
DS 5 1.35 12 1.35 0.05 26
DS 5 -1.35 5 -2.9 0.05 26
DS 5 1.35 5 2.9 0.05 26
$PAD
Sh "C" R 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -10.6 0
$EndPAD
$PAD
Sh "A" C 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 10.6 0
$EndPAD
$EndMODULE DIOAD2120W130L950D530P
